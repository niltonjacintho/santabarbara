import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documents/repository/DocumentsRepository.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

part 'documents_controller.g.dart';

class DocumentsController = _DocumentsControllerBase with _$DocumentsController;

abstract class _DocumentsControllerBase with Store {
  UtilsController util = Modular.get();
  @observable
  DocumentsModel documents;

  @observable
  List listaPastorais = [];

  @observable
  String filterPastorais = '';

  @observable
  String filterArtigos = '';

  @observable
  List questionsList = [];

  set setDocuments(DocumentsModel value) => documents = value;
  DocumentsModel get getDocuments => documents;

  DocumentsRepository documentsRepository = new DocumentsRepository();
  Stream<QuerySnapshot> getStreamList(Category categoria) {
    return documentsRepository.xGetStream();
  }

  Stream<QuerySnapshot> xGetStreamList(Category categoria) {
    return documentsRepository.xGetStream();
  }

  Stream<QuerySnapshot> getStreamAvisos() {
    Stream<QuerySnapshot> snap = Firestore.instance
        .collection('artigos')
        .where('ativo', isEqualTo: true)
        .snapshots();
    return snap;
  }

  Stream<QuerySnapshot> getStreamByGroup(int group) {
    var query = Firestore.instance.collection('artigos');
    if (filterArtigos != '') {
      query = query.where('titulo', isGreaterThanOrEqualTo: filterPastorais);
    }
    return query
        .where('grupo', isEqualTo: group.toString())
        .where('ativo', isEqualTo: true)
        .snapshots();
    // return snap;
  }

  Future<QuerySnapshot> getDocumentsByGroup(int group) {
    Future<QuerySnapshot> doc = Firestore.instance
        .collection('artigos')
        .where('grupo', isEqualTo: group.toString())
        .where('ativo', isEqualTo: true)
        .getDocuments();

    doc.then((d) => {
          print(d),
          documents = new DocumentsModel(),
          documents.id = d.documents[0].data['id'],
          documents.titulo = d.documents[0].data['titulo'],
          documents.subtitulo = d.documents[0].data['subtitulo'],
          documents.conteudo = d.documents[0].data['conteudo'],
          documents.imagem = d.documents[0].data['imagem'],
        });
    return doc;
  }

  @action
  Future<List> getPastorais() async {
    var snap = Firestore.instance
        .collection('pastorais')
        .orderBy("titulo")
        .snapshots();
    listaPastorais = [];
    snap.forEach((field) {
      field.documents.asMap().forEach((index, data) {
        // print(data);
        if (dadoValidoPesquisa(data['titulo']) |
            dadoValidoPesquisa(data['sigla'])) {
          listaPastorais.add(data);
        }
      });
    });
    // print(listaPastorais);
    return listaPastorais;
  }

  bool dadoValidoPesquisa(value) {
    return (value
                .toString()
                .toLowerCase()
                .indexOf(filterPastorais.toLowerCase()) !=
            -1) |
        (filterPastorais == '');
  }

  //@action
  getQuestions() async {
    print('GETTING QUESTIONS');
    Future<QuerySnapshot> snap = Firestore.instance
        .collection('artigos')
        //   .where('grupo', isEqualTo: '700')
        //   .where('ativo', isEqualTo: true)
        .getDocuments();
    //   .then((onValue) => print('ONVALUE $onValue'));
    int i = 0;
    try {
      await snap.then(
        (doc) => {
          questionsList = [],
          for (DocumentSnapshot item in doc.documents)
            {
              if (item.data['subtitulo'] != null)
                {
                  questionsList.add(
                    {
                      '\'name\'': '\'${item.data['titulo']}\'',
                      '\'group\'': '\'${item.data['subtitulo']}\''
                    },
                  ),
                  print(i),
                  i++,
                }
            },
        },
      );
    } catch (e) {
      print('error $e');
    }
    questionsList = [
      {'name': 'Retornoi', 'group': 'Team A'},
      {'name': 'Will', 'group': 'Team B'},
      {'name': 'Beth', 'group': 'Team A'},
      {'name': 'Miranda', 'group': 'Team B'},
      {'name': 'dfgdfgdfgdgdfgdfgdgdf', 'group': 'Team C'},
      {'name': 'Danny', 'group': 'Team C'},
    ];
  }
}
