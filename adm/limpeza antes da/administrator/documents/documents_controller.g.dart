// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'documents_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DocumentsController on _DocumentsControllerBase, Store {
  final _$documentsAtom = Atom(name: '_DocumentsControllerBase.documents');

  @override
  DocumentsModel get documents {
    _$documentsAtom.context.enforceReadPolicy(_$documentsAtom);
    _$documentsAtom.reportObserved();
    return super.documents;
  }

  @override
  set documents(DocumentsModel value) {
    _$documentsAtom.context.conditionallyRunInAction(() {
      super.documents = value;
      _$documentsAtom.reportChanged();
    }, _$documentsAtom, name: '${_$documentsAtom.name}_set');
  }

  final _$listaPastoraisAtom =
      Atom(name: '_DocumentsControllerBase.listaPastorais');

  @override
  List<dynamic> get listaPastorais {
    _$listaPastoraisAtom.context.enforceReadPolicy(_$listaPastoraisAtom);
    _$listaPastoraisAtom.reportObserved();
    return super.listaPastorais;
  }

  @override
  set listaPastorais(List<dynamic> value) {
    _$listaPastoraisAtom.context.conditionallyRunInAction(() {
      super.listaPastorais = value;
      _$listaPastoraisAtom.reportChanged();
    }, _$listaPastoraisAtom, name: '${_$listaPastoraisAtom.name}_set');
  }

  final _$filterPastoraisAtom =
      Atom(name: '_DocumentsControllerBase.filterPastorais');

  @override
  String get filterPastorais {
    _$filterPastoraisAtom.context.enforceReadPolicy(_$filterPastoraisAtom);
    _$filterPastoraisAtom.reportObserved();
    return super.filterPastorais;
  }

  @override
  set filterPastorais(String value) {
    _$filterPastoraisAtom.context.conditionallyRunInAction(() {
      super.filterPastorais = value;
      _$filterPastoraisAtom.reportChanged();
    }, _$filterPastoraisAtom, name: '${_$filterPastoraisAtom.name}_set');
  }

  final _$filterArtigosAtom =
      Atom(name: '_DocumentsControllerBase.filterArtigos');

  @override
  String get filterArtigos {
    _$filterArtigosAtom.context.enforceReadPolicy(_$filterArtigosAtom);
    _$filterArtigosAtom.reportObserved();
    return super.filterArtigos;
  }

  @override
  set filterArtigos(String value) {
    _$filterArtigosAtom.context.conditionallyRunInAction(() {
      super.filterArtigos = value;
      _$filterArtigosAtom.reportChanged();
    }, _$filterArtigosAtom, name: '${_$filterArtigosAtom.name}_set');
  }

  final _$questionsListAtom =
      Atom(name: '_DocumentsControllerBase.questionsList');

  @override
  List<dynamic> get questionsList {
    _$questionsListAtom.context.enforceReadPolicy(_$questionsListAtom);
    _$questionsListAtom.reportObserved();
    return super.questionsList;
  }

  @override
  set questionsList(List<dynamic> value) {
    _$questionsListAtom.context.conditionallyRunInAction(() {
      super.questionsList = value;
      _$questionsListAtom.reportChanged();
    }, _$questionsListAtom, name: '${_$questionsListAtom.name}_set');
  }

  final _$getPastoraisAsyncAction = AsyncAction('getPastorais');

  @override
  Future<List<dynamic>> getPastorais() {
    return _$getPastoraisAsyncAction.run(() => super.getPastorais());
  }

  @override
  String toString() {
    final string =
        'documents: ${documents.toString()},listaPastorais: ${listaPastorais.toString()},filterPastorais: ${filterPastorais.toString()},filterArtigos: ${filterArtigos.toString()},questionsList: ${questionsList.toString()}';
    return '{$string}';
  }
}
