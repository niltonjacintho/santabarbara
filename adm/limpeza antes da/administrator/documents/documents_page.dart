import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documentsmanager/documentsmanager_page.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'documents_controller.dart';

class DocumentsPage extends StatefulWidget {
  final String title;
  const DocumentsPage({Key key, this.title = "Documents"}) : super(key: key);

  @override
  _DocumentsPageState createState() => _DocumentsPageState();
}

class _DocumentsPageState
    extends ModularState<DocumentsPage, DocumentsController> {
  Stream<QuerySnapshot> stream;
//  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
  }

  //use 'controller' variable to access controller

  UtilsController util = Modular.get();
  // Category c = util.categoryAvisos();
  @override
  Widget build(BuildContext context) {
    DocumentsController documentsController = Modular.get();
    Stream<QuerySnapshot> myStream = documentsController.getStreamAvisos();
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              documentsController.setDocuments =
                  new DocumentsModel().newDocument();
              return MyHomePage();
            },
          );
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
      body: Container(
        color: util.corFundoAvisos,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text('Paróquia Santa Barbara'),
              backgroundColor: Colors.green,
              expandedHeight: 200,
              floating: true,
              pinned: true,
              snap: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text('Lista de Avisos',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    )),
                background: Image.asset(
                  'assets/images/paroquiadrone.jpeg',
                  fit: BoxFit.cover,
                ),
                //  Text('2222'),
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: myStream,
              builder: (context, snapshot) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                leading: Container(
                                  decoration: new BoxDecoration(
                                      color: Colors
                                          .green, //new Color.fromRGBO(255, 0, 0, 0.0),
                                      borderRadius: new BorderRadius.only(
                                          topLeft: const Radius.circular(20.0),
                                          topRight:
                                              const Radius.circular(20.0))),
                                  // color: Colors.red,
                                  child: Image.network(
                                    snapshot
                                        .data.documents[index].data['imagem'],
                                    fit: BoxFit.fitHeight,
                                    alignment: Alignment
                                        .center, // .centerLeft, // .topCenter,
                                  ),
                                  height: 100,
                                  width: 100,
                                ),
                                onTap: () {
                                  documentsController.setDocuments =
                                      convertToDoc(
                                          snapshot.data.documents[index].data);
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return MyHomePage();
                                    },
                                  );
                                },
                                title: Text(
                                  snapshot.data.documents[index].data['titulo'],
                                  style: TextStyle(fontSize: 24),
                                ),
                                subtitle: Text(
                                    snapshot.data.documents[index]
                                        .data['subtitulo'],
                                    style: TextStyle(fontSize: 16)),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    childCount:
                        snapshot.hasData ? snapshot.data.documents.length : 0,
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  DocumentsModel convertToDoc(Map<String, dynamic> value) {
    DocumentsModel doc = new DocumentsModel();
    doc.titulo = value['titulo'];
    doc.subtitulo = value['subtitulo'];
    doc.conteudo = value['conteudo'];
    doc.imagem = value['imagem'];
    doc.ativo = true;
    if (value['id'] != null) {
      doc.id = value["id"];
    } else {
      doc.id = new DateTime.now().millisecondsSinceEpoch.toString();
    }
    return doc;
  }
}

Widget box(BuildContext context) {
  return RaisedButton(
    child: Text("Alert - Tap to dismiss"),
    color: Colors.yellow,
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
}
