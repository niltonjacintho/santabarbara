import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_page.dart';

class DocumentsModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DocumentsController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => DocumentsPage()),
      ];

  static Inject get to => Inject<DocumentsModule>.of();
}
