import 'package:santabarbara/app/modules/administrator/documentsmanager/documentsmanager_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/administrator/documentsmanager/documentsmanager_page.dart';

class DocumentsmanagerModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DocumentsmanagerController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => MyHomePage()),
      ];

  static Inject get to => Inject<DocumentsmanagerModule>.of();
}
