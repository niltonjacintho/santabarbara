import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:material_dialog/material_dialog.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/services/imageservice_service.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter FormBuilder Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  MyHomePageState createState() {
    return MyHomePageState();
  }
}

class MyHomePageState extends State<MyHomePage> {
  // var data;
  final imageController = TextEditingController();
  // bool autoValidate = true;
  // bool readOnly = false;
  // bool showSegmentedControl = true;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  DocumentsController documentsController = Modular.get();
  ImageserviceService _imageService = new ImageserviceService();
  UtilsController util = Modular.get();
  AuthController auth = Modular.get();
  var textoacao = 'Inclusão';
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: MaterialDialog(
          enableCloseButton: true,
          onCloseButtonClicked: () {
            Navigator.pop(context);
          },
          title: Text('Adicionando avisos'),
          subTitle:
              Text('Todos os campos são obrigatórios, inclusive a imagem.',
                  style: TextStyle(
                    color: Colors.red,
                  )),
          children: <Widget>[
            FormBuilder(
              // context,
              key: _fbKey,
              autovalidate: true,
              initialValue: {
                'titulo': documentsController.documents.titulo,
                'subtitulo': documentsController.documents.subtitulo,
                'conteudo': documentsController.documents.conteudo,
                'grupo': documentsController.documents.grupo,
              },
              readOnly: false,
              child: Column(
                children: <Widget>[
                  FormBuilderDropdown(
                    attribute: "grupo",
                    decoration: InputDecoration(
                      labelText: "Tipo do documento",
                      hintStyle: new TextStyle(fontSize: 120),
                    ),
                    // initialValue: 'Male',
                    hint: Text('Selecione o tipo do documento'),
                    validators: [FormBuilderValidators.required()],
                    items: util.getCategoryList(),
                    initialValue: util.getDropDownMenuItem(100).value,
                    onChanged: (val) => {
                      documentsController.documents.grupo = val.toString(),
                    },
                  ),
                  FormBuilderTextField(
                    attribute: "titulo",
                    decoration: InputDecoration(
                      labelText: "Título",
                    ),
                    onChanged: (val) => {
                      documentsController.documents.titulo = val,
                      gravar(context),
                      print(val),
                    },
                    // validators: [
                    //   FormBuilderValidators.required(),
                    // ],
                    keyboardType: TextInputType.text,
                  ),
                  FormBuilderTextField(
                    attribute: "subtitulo",
                    decoration: InputDecoration(
                      labelText: "Sub título",
                    ),
                    onChanged: (val) => {
                      documentsController.documents.subtitulo = val,
                      gravar(context),
                      print(val),
                    },
                    // validators: [
                    //   FormBuilderValidators.required(),
                    // ],
                    keyboardType: TextInputType.text,
                  ),
                  FormBuilderTextField(
                    attribute: "conteudo",
                    minLines: 5,
                    maxLines: 10,
                    decoration: InputDecoration(
                      labelText: "Conteúdo",
                    ),
                    onChanged: (val) => {
                      documentsController.documents.conteudo = val,
                      gravar(context),
                      print(val),
                    },
                    // validators: [
                    //   FormBuilderValidators.required(),
                    // ],
                    keyboardType: TextInputType.multiline,
                  ),
                  FormBuilderTextField(
                    controller: imageController,
                    attribute: "imagem",
                    //  controller: _username,
                    decoration: InputDecoration(
                        hintText: 'url da imagem',
                        labelText: "Imagem",
                        suffixIcon: IconButton(
                            icon: Icon(Icons.import_export),
                            onPressed: () {
                              DocumentsModel doc;
                              _imageService.chooseFile().then(
                                    (value) => {
                                      doc = documentsController.documents,
                                      doc.imagem = value.uri.path,
                                      documentsController.setDocuments = doc,
                                      gravarImage(),
                                      print('DOC ATUALIZADO $doc'),
                                      _fbKey.currentState.value['imagem'] =
                                          'sss', // imageController.value = doc.imagem,
                                    },
                                  );
                            })),
                  ),
                  //      ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Observer(
                  builder: (_) =>
                      documentsController.documents.imagem.indexOf('https') != 0
                          ? Image.file(
                              new File(
                                  documentsController.documents.imagem != null
                                      ? documentsController.documents.imagem
                                      : ''),
                              fit: BoxFit.fitHeight,
                              alignment: Alignment.topCenter,
                            )
                          : Image.network(documentsController.documents.imagem),
                ),
                //  ),
              ],
            )
            //   ),
          ],
        ),
      ),
      //    ),
    );
  }

  void gravar(BuildContext context) async {
    documentsController.documents.ativo = true;
    DocumentReference reference = Firestore.instance
        .document("artigos/" + documentsController.documents.id);
    reference
        .setData(documentsController.documents.toMap())
        .then((result) => {})
        .catchError((err) => print('ERROR $err'));
    // });
  }

  gravarImage() {
    print('GRUPO PARA ENVIAR IMAGEM ${documentsController.documents.grupo}');
    _imageService.uploadFile(documentsController.documents.grupo).then(
      (onValue) {
        documentsController.documents.imagem = _imageService.url;
        gravar(context);
      },
    );
  }
}
