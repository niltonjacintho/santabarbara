import 'package:mobx/mobx.dart';

part 'documentsmanager_controller.g.dart';

class DocumentsmanagerController = _DocumentsmanagerControllerBase
    with _$DocumentsmanagerController;

abstract class _DocumentsmanagerControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
