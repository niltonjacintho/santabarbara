import 'package:firebase/firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase/firebase.dart' as fb;
// import 'package:admFlutter/shared/user.repository.dart';

void main() {
  runApp(MyApp());
  print('inicializando');
  if (fb.apps.isEmpty) {
    fb.initializeApp(
        apiKey: "AIzaSyBINKWY8hXzMKgH20758sBgoqCKu6YuJ-Q",
        authDomain: "paroquia.firebaseapp.com",
        databaseURL: "https://paroquia.firebaseio.com",
        projectId: "project-2297216869628270192",
        storageBucket: "project-2297216869628270192.appspot.com",
        messagingSenderId: "999243580674");
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  // UserRepository userRepository = new UserRepository();
  //AuthService authService = new AuthService();
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            RaisedButton(
              onPressed: () async {
                //   await authService.signIn('email', 'password');
                //    fb.firestore().collection('logs').add({"data": "teste"});
                //   userRepository.signInWithGoogle();
              },
              child: Text('Login'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
