const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.helloWorld = functions.database.ref('artigos/{id}').onWrite((evt: any) => {
    console.log('evento ', evt);
    const payload = {
        notification: {
            title: 'Novo aplicativo Paroquia Santa Barbara',
            body: 'Pessoal é apenas um teste',
            badge: '1',
            sound: 'default'
        }
    };

    return admin.database().ref('artigos').once('value').then((allToken: any) => {
        if (allToken.val()) {
            console.log('token available');
            //   const token = Object.keys(allToken.val());
            return admin.messaging().sendAll(payload)
                .then((value: any) => {
                    console.log(value)
                })
                .catch((err: any) => {
                    console.error(err);
                })

        } else {
            console.log('No token available');
        }
    });
});