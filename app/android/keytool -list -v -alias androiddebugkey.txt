keytool -list -v -alias androiddebugkey -keystore %USERPROFILE%\.android\debug.keystore

keytool -genkey -alias androiddebugkey -keyalg RSA -keystore %USERPROFILE%\.android\debug.keystore -keysize 2048

keytool -exportcert -list -v -alias androiddebugkey -keystore  %USERPROFILE%\.android\debug.keystore