import 'package:santabarbara/app/modules/agenda/agenda_page.dart';
import 'package:santabarbara/app/modules/artigo/artigo_page.dart';
import 'package:santabarbara/app/modules/home/home_page.dart';
import 'package:santabarbara/app/modules/horarios/horarios_page.dart';
import 'package:santabarbara/app/modules/login/login_controller.dart';
import 'package:santabarbara/app/modules/login/login_page.dart';
import 'package:santabarbara/app/modules/map/map_page.dart';
import 'package:santabarbara/app/modules/padre/padre/padre_page.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_page.dart';
import 'package:santabarbara/app/modules/pastorais/pastorais_page.dart';
import 'package:santabarbara/app/modules/pedidos/pedidos_page.dart';
import 'package:santabarbara/app/modules/perguntas/perguntas_page.dart';
import 'package:santabarbara/app/modules/pessoas/pessoas_page.dart';
import 'package:santabarbara/app/modules/quiz/quiz_page.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_page.dart';

import 'pages/oracao/oracao_controller.dart';
import 'store/base_store.dart';
// import 'shared/grupo_service.dart';
import 'shared/grupos_service.dart';
// import 'shared/grupos_service_service.dart';
import 'pages/artigoView/artigo_view_controller.dart';
// import 'pages/artigo/artigo_controller.dart';
import 'package:santabarbara/app/modules/pessoas/pessoas_module.dart';
import 'package:santabarbara/app/modules/quiz/quizFinal/quiz_final_page.dart';
import 'package:santabarbara/app/modules/quiz/quizTop/quiz_top_controller.dart';
import 'package:santabarbara/app/modules/quiz/quizTop/quiz_top_page.dart';
import 'package:santabarbara/app/modules/quiz/quiz_controller.dart';
import 'package:santabarbara/app/modules/velavirtual/velaacesa/velaacesa_page.dart';
import 'package:santabarbara/app/modules/horarios/horarios_module.dart';

import 'services/db_service.dart';
import 'package:santabarbara/app/modules/quiz/quizGame/quiz_game_page.dart';
import 'package:santabarbara/app/modules/quiz/quiz_module.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_module.dart';
import 'package:santabarbara/app/pages/vela/vela_controller.dart';

import 'package:santabarbara/app/modules/oracoes_comuns/oracoes_comuns_page.dart';
import 'package:santabarbara/app/modules/oracoes_comuns/oracoes_comuns_controller.dart';
import 'package:santabarbara/app/modules/paroquias/capelas/capelas_page.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_module.dart';
import 'package:santabarbara/app/modules/pedidos/pedidos_module.dart';
import 'package:santabarbara/app/modules/pastorais/pastorais_controller.dart';
import 'package:santabarbara/app/modules/pastorais/pastorais_module.dart';
import 'package:santabarbara/app/modules/perguntas/perguntas_module.dart';
// import 'package:santabarbara/app/modules/saudades/saudades_controller.dart';
// import 'package:santabarbara/app/modules/saudades/saudades_module.dart';
import 'package:santabarbara/app/pages/vela/vela_page.dart';
import 'package:santabarbara/app/services/imageservice_service.dart';
// import 'package:santabarbara/app/modules/administrator/documentsmanager/documentsmanager_controller.dart';
// import 'package:santabarbara/app/modules/administrator/documents/documents_module.dart';
import 'package:santabarbara/app/pages/slide_bar/slide_bar_controller.dart';
import 'package:santabarbara/app/modules/padre/padre/padre_module.dart';
import 'package:santabarbara/app/modules/padre/repositories/padre_repository.dart';
import 'package:santabarbara/app/modules/agenda/agenda_module.dart';
import 'package:santabarbara/app/modules/artigo/artigo_module.dart';
import 'package:santabarbara/app/modules/login/login_module.dart';
import 'package:santabarbara/app/pages/splash/splash_controller.dart';
import 'package:santabarbara/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:santabarbara/app/app_widget.dart';
import 'package:santabarbara/app/modules/home/home_module.dart';
// import 'package:santabarbara/app/modules/noticiasCatolicas/noticias_catolicas_module.dart';
import 'package:santabarbara/app/modules/noticiasCatolicas/noticias_catolicas_controller.dart';
import 'package:santabarbara/app/modules/noticiasCatolicas/noticias_catolicas_page.dart';
import 'package:santabarbara/app/modules/map/map_module.dart';
import 'package:santabarbara/app/modules/map/map_controller.dart';
import 'package:santabarbara/app/modules/remedios/remedios_controller.dart';
import 'package:santabarbara/app/pages/splash/splash_page.dart';
import 'package:santabarbara/app/pages/artigoView/artigo_view_controller.dart';
import 'package:santabarbara/app/pages/oracao/oracao_page.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/auth/repositories/auth_repository.dart';
import 'package:santabarbara/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:flutter/cupertino.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        // $GrupoService,
        // $GruposService,
        // $GruposServiceService,
        //  $ArtigoViewController,
        // $ArtigoController,
        Bind((i) => DbService()),
        Bind((i) => GruposService()),
        Bind((i) => VelaController()),
        Bind((i) => RemediosController()),
        Bind((i) => OracaoController()),
        Bind((i) => MapController()),
        Bind((i) => ImageserviceService()),
        Bind((i) => LoginController()),
        Bind((i) => SlideBarController()),
        Bind((i) => PadreRepository()),
        Bind((i) => SplashController()),
        Bind((i) => AppController()),
        // Bind((i) => SaudadesController()),
        Bind((i) => OracoesComunsController()),
        Bind((i) => ArtigoViewController()),
        Bind<IAuthRepository>((i) => AuthRepository()),
        Bind((i) => AuthController()),
        Bind((i) => PastoraisController()),
        Bind((i) => QuizTopController()),
        Bind((i) => QuizController()),
        Bind((i) => UtilsController()),
        Bind((i) => BaseStore()),
        Bind((i) => NoticiasCatolicasController()),
      ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute('/home', module: HomeModule()),
    ModuleRoute('/', module: LoginModule()),
  ];

  // List<ModularRoute> get routers => [
  //       ChildRoute(Modular.initialRoute, child: (_, args) => SplashPage()),
  //       ChildRoute('/login', child: (_, args) => LoginPage()),
  //       ChildRoute('/artigo',
  //           child: (_, args) => ArtigosPage(), transition: TransitionType.size),
  //       ChildRoute('/velavirtual',
  //           child: (_, args) => VelavirtualPage(),
  //           transition: TransitionType.rotate),
  //       ChildRoute('/agenda',
  //           child: (_, args) => AgendaPage(),
  //           transition: TransitionType.leftToRight),
  //       ChildRoute('/home',
  //           child: (_, args) => HomePage(), transition: TransitionType.fadeIn),
  //       ChildRoute('/padres',
  //           child: (_, args) => PadrePage(), transition: TransitionType.fadeIn),
  //       // Router('/Docs',
  //       //     module: DocumentsModule(), transition: TransitionType.scale),
  //       ChildRoute('/perguntas',
  //           child: (_, args) => PerguntasPage(),
  //           transition: TransitionType.scale),
  //       ChildRoute('/pastorais',
  //           child: (_, args) => PastoraisPage(),
  //           transition: TransitionType.scale),
  //       // Router('/saudades',
  //       //     module: SaudadesModule(), transition: TransitionType.rotate),
  //       ChildRoute('/horarios',
  //           child: (_, args) => HorariosPage(),
  //           transition: TransitionType.rotate),
  //       ChildRoute('/paroquias',
  //           child: (_, args) => ParoquiasPage(),
  //           transition: TransitionType.scale),
  //       ChildRoute('/pessoais',
  //           child: (_, args) => PessoasPage(),
  //           transition: TransitionType.scale),

  //       ChildRoute('/maps',
  //           child: (_, args) => MapPage(), transition: TransitionType.scale),
  //       ChildRoute('/pedidos',
  //           child: (_, args) => PedidosPage(),
  //           transition: TransitionType.scale),
  //       ChildRoute('/quiz',
  //           child: (_, args) => QuizPage(), transition: TransitionType.scale),
  //       ChildRoute('/capelas', child: (_, args) => CapelasPage()),
  //       ChildRoute('/vela', child: (_, args) => VelaPage()),
  //       ChildRoute('/quizpage', child: (_, args) => QuizGamePage()),
  //       ChildRoute('/quizpagefinal', child: (_, args) => QuizFinalPage()),
  //       ChildRoute('/quiztop', child: (_, args) => QuizTopPage()),
  //       ChildRoute('/velaacendendo', child: (_, args) => VelaacesaPage()),
  //       ChildRoute('/oracoes_comuns', child: (_, args) => OracoesComunsPage()),
  //       ChildRoute('/oracaoView', child: (_, args) => OracaoPage()),
  //       ChildRoute('/noticias_catolicas',
  //           child: (_, args) => NoticiasCatolicasPage()),
  //     ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>();
}
