import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/formGenerator_model.dart';

part 'form_generator_controller.g.dart';

class FormGeneratorController = _FormGeneratorControllerBase
    with _$FormGeneratorController;

abstract class _FormGeneratorControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  @observable
  FormsBase formAtual = new FormsBase();

  @observable
  Fields actualField = new Fields();

  @observable
  bool fieldIdValid = true;

  @action
  showListView() {}

  @action
  showForm() {}

  @action
  showFormElement(int elementPosition) {}

  @action
  saveDefault() {}
}
