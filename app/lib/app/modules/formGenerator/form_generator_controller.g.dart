// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_generator_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FormGeneratorController on _FormGeneratorControllerBase, Store {
  final _$valueAtom = Atom(name: '_FormGeneratorControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$formAtualAtom = Atom(name: '_FormGeneratorControllerBase.formAtual');

  @override
  FormsBase get formAtual {
    _$formAtualAtom.reportRead();
    return super.formAtual;
  }

  @override
  set formAtual(FormsBase value) {
    _$formAtualAtom.reportWrite(value, super.formAtual, () {
      super.formAtual = value;
    });
  }

  final _$actualFieldAtom =
      Atom(name: '_FormGeneratorControllerBase.actualField');

  @override
  Fields get actualField {
    _$actualFieldAtom.reportRead();
    return super.actualField;
  }

  @override
  set actualField(Fields value) {
    _$actualFieldAtom.reportWrite(value, super.actualField, () {
      super.actualField = value;
    });
  }

  final _$fieldIdValidAtom =
      Atom(name: '_FormGeneratorControllerBase.fieldIdValid');

  @override
  bool get fieldIdValid {
    _$fieldIdValidAtom.reportRead();
    return super.fieldIdValid;
  }

  @override
  set fieldIdValid(bool value) {
    _$fieldIdValidAtom.reportWrite(value, super.fieldIdValid, () {
      super.fieldIdValid = value;
    });
  }

  final _$_FormGeneratorControllerBaseActionController =
      ActionController(name: '_FormGeneratorControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_FormGeneratorControllerBaseActionController
        .startAction(name: '_FormGeneratorControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_FormGeneratorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic showListView() {
    final _$actionInfo = _$_FormGeneratorControllerBaseActionController
        .startAction(name: '_FormGeneratorControllerBase.showListView');
    try {
      return super.showListView();
    } finally {
      _$_FormGeneratorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic showForm() {
    final _$actionInfo = _$_FormGeneratorControllerBaseActionController
        .startAction(name: '_FormGeneratorControllerBase.showForm');
    try {
      return super.showForm();
    } finally {
      _$_FormGeneratorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic showFormElement(int elementPosition) {
    final _$actionInfo = _$_FormGeneratorControllerBaseActionController
        .startAction(name: '_FormGeneratorControllerBase.showFormElement');
    try {
      return super.showFormElement(elementPosition);
    } finally {
      _$_FormGeneratorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic saveDefault() {
    final _$actionInfo = _$_FormGeneratorControllerBaseActionController
        .startAction(name: '_FormGeneratorControllerBase.saveDefault');
    try {
      return super.saveDefault();
    } finally {
      _$_FormGeneratorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
formAtual: ${formAtual},
actualField: ${actualField},
fieldIdValid: ${fieldIdValid}
    ''';
  }
}
