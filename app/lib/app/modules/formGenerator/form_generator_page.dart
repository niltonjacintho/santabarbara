import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'form_generator_controller.dart';

class FormGeneratorPage extends StatefulWidget {
  final String title;
  const FormGeneratorPage({Key key, this.title = "FormGenerator"})
      : super(key: key);

  @override
  _FormGeneratorPageState createState() => _FormGeneratorPageState();
}

class _FormGeneratorPageState
    extends ModularState<FormGeneratorPage, FormGeneratorController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
