import 'form_generator_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'form_generator_page.dart';

class FormGeneratorModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => FormGeneratorController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => FormGeneratorPage()),
      ];

  static Inject get to => Inject<FormGeneratorModule>(); //>.of(;
}
