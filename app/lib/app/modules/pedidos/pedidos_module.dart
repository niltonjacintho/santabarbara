import 'package:santabarbara/app/modules/pedidos/pedidos_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/pedidos/pedidos_page.dart';

class PedidosModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PedidosController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => PedidosPage()),
      ];

  static Inject get to => Inject<PedidosModule>(); //>.of(;
}
