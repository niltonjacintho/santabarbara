import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

import 'pedidos_controller.dart';

class PedidosPage extends StatefulWidget {
  final String title;
  const PedidosPage({Key key, this.title = "Pedidos"}) : super(key: key);

  @override
  _PedidosPageState createState() => _PedidosPageState();
}

class _PedidosPageState extends ModularState<PedidosPage, PedidosController> {
  //use 'controller' variable to access controller
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final format = DateFormat("dd/MM/yyyy");
  PedidosController pedidosController = Modular.get();
  @override
  Widget build(BuildContext context) {
    var _selectedIndex = 0;
    pedidosController.getTiposPedidos();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: (BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.send, size: 30),
            label: 'Enviar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.cancel),
            label: 'Não Enviar',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (value) async {
          if (value == 0) {
            if (_fbKey.currentState.saveAndValidate()) {
              print(_fbKey.currentState.value);
              await pedidosController.gravar();
              Modular.to.pop();
            }
          } else {
            Modular.to.pop();
          }
          print(value);
        },
      )),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            FormBuilder(
              // context,
              key: _fbKey,
           //   readOnly: false,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 15),
                  FormBuilderTextField(
                    name: 'para',
                //    attribute: "para",
                    decoration: InputDecoration(
                      labelText: "Você esta pedindo para quem?",
                    ),
                    onChanged: (value) {
                      pedidosController.pedidoAtual.para = value;
                    },
                    valueTransformer: (text) {
                      return text == null ? null : num.tryParse(text);
                    },
                    // validator: [
                    //   FormBuilderValidators.required(
                    //       errorText: 'Poxa, nós precisamos desta informação!'),
                    //   // FormBuilderValidators.max(70),
                    //   FormBuilderValidators.minLength(2, allowEmpty: false),
                    // ],
                    keyboardType: TextInputType.number,
                  ),
                  FormBuilderDateTimePicker(
                    name: "data",
                    // onChanged: (value) {
                    //   pedidosController.pedidoAtual.data = value;
                    // },
                    inputType: InputType.date,
                    decoration: InputDecoration(
                      labelText: "Para quando é este pedido?",
                    ),
                    // validators: [
                    //   FormBuilderValidators.required(
                    //       errorText: 'Poxa, nós precisamos desta informação!'),
                    // ],
                    //initialTime:  TimeOfDay(hour: 8, minute: 0),
                    initialValue: DateTime.now(),
                    // readonly: true,
                  ),
                  FormBuilderTextField(
                    name: "texto",
                    maxLines: 5,
                    decoration: InputDecoration(
                      labelText: "Escreva o seu pedido.",
                    ),
                    onChanged: (value) {
                      pedidosController.pedidoAtual.texto = value;
                    },
                    valueTransformer: (text) {
                      return text == null ? null : num.tryParse(text);
                    },
                    // validators: [
                    //   FormBuilderValidators.required(
                    //       errorText: 'Poxa, nós precisamos desta informação!'),
                    //   FormBuilderValidators.minLength(5,
                    //       allowEmpty: false,
                    //       errorText:
                    //           'Aqui você precisa pelo menos 5 caracteres!'),
                    // ],
                    keyboardType: TextInputType.text,
                  ),
                  Observer(
                    builder: (_) => FormBuilderDropdown(
                      name: "tipo",
                      decoration: InputDecoration(
                        labelText: "O pedido é sobre ?",
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.green,
                            width: 20,
                          ),
                        ),
                      ),
                      onChanged: (value) {
                        pedidosController.pedidoAtual.tipo = value;
                      },
                      // validators: [
                      //   FormBuilderValidators.required(
                      //       errorText: 'Nós precisamos desta informação!')
                      // ],
                      items: pedidosController.listaTipos
                          .map((tipo) => DropdownMenuItem(
                                value: tipo,
                                child: Text('$tipo'),
                              ))
                          .toList(),
                    ),
                  ),
                  SizedBox(height: 15),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
