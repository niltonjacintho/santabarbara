import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/pedidos_model.dart';
import 'package:uuid/uuid.dart';

part 'pedidos_controller.g.dart';

class PedidosController = _PedidosControllerBase with _$PedidosController;

abstract class _PedidosControllerBase with Store {
  @observable
  PedidosModel pedidoAtual = new PedidosModel();

  @observable
  int value = 0;

  @observable
  List listaTipos = [];

  @action
  void increment() {
    value++;
  }

  var uuid = Uuid();

  @action
  getTiposPedidos() async {
    QuerySnapshot qShot =
        await FirebaseFirestore.instance.collection('pedidos_tipos').get();
    listaTipos = qShot.docs.map((doc) => doc.get('tipo')).toList();
    return qShot.docs.map((doc) => doc.get('tipo')).toList();
  }

  gravar() async {
    if (pedidoAtual.id == null || pedidoAtual.id == '') {
      pedidoAtual.id = uuid.v4();
    }
    DocumentReference reference =
        FirebaseFirestore.instance.doc("pedidos/" + pedidoAtual.id);
    pedidoAtual.dtpedido =
        pedidoAtual.dtpedido == null ? DateTime.now() : pedidoAtual.dtpedido;
    reference
        .set(pedidoAtual.toJson())
        .then((result) => {})
        .catchError((err) => print('ERROR $err'));
    // });
  }
}
