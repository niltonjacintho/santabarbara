// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pedidos_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PedidosController on _PedidosControllerBase, Store {
  final _$pedidoAtualAtom = Atom(name: '_PedidosControllerBase.pedidoAtual');

  @override
  PedidosModel get pedidoAtual {
    _$pedidoAtualAtom.reportRead();
    return super.pedidoAtual;
  }

  @override
  set pedidoAtual(PedidosModel value) {
    _$pedidoAtualAtom.reportWrite(value, super.pedidoAtual, () {
      super.pedidoAtual = value;
    });
  }

  final _$valueAtom = Atom(name: '_PedidosControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$listaTiposAtom = Atom(name: '_PedidosControllerBase.listaTipos');

  @override
  List<dynamic> get listaTipos {
    _$listaTiposAtom.reportRead();
    return super.listaTipos;
  }

  @override
  set listaTipos(List<dynamic> value) {
    _$listaTiposAtom.reportWrite(value, super.listaTipos, () {
      super.listaTipos = value;
    });
  }

  final _$getTiposPedidosAsyncAction =
      AsyncAction('_PedidosControllerBase.getTiposPedidos');

  @override
  Future getTiposPedidos() {
    return _$getTiposPedidosAsyncAction.run(() => super.getTiposPedidos());
  }

  final _$_PedidosControllerBaseActionController =
      ActionController(name: '_PedidosControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_PedidosControllerBaseActionController.startAction(
        name: '_PedidosControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_PedidosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
pedidoAtual: ${pedidoAtual},
value: ${value},
listaTipos: ${listaTipos}
    ''';
  }
}
