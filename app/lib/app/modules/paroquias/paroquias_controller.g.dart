// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paroquias_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ParoquiasController on _ParoquiasControllerBase, Store {
  final _$listaAtom = Atom(name: '_ParoquiasControllerBase.lista');

  @override
  List<Igreja> get lista {
    _$listaAtom.reportRead();
    return super.lista;
  }

  @override
  set lista(List<Igreja> value) {
    _$listaAtom.reportWrite(value, super.lista, () {
      super.lista = value;
    });
  }

  final _$totalParoquiasAtom =
      Atom(name: '_ParoquiasControllerBase.totalParoquias');

  @override
  int get totalParoquias {
    _$totalParoquiasAtom.reportRead();
    return super.totalParoquias;
  }

  @override
  set totalParoquias(int value) {
    _$totalParoquiasAtom.reportWrite(value, super.totalParoquias, () {
      super.totalParoquias = value;
    });
  }

  final _$contParoquiaAtom =
      Atom(name: '_ParoquiasControllerBase.contParoquia');

  @override
  int get contParoquia {
    _$contParoquiaAtom.reportRead();
    return super.contParoquia;
  }

  @override
  set contParoquia(int value) {
    _$contParoquiaAtom.reportWrite(value, super.contParoquia, () {
      super.contParoquia = value;
    });
  }

  final _$filtroAtom = Atom(name: '_ParoquiasControllerBase.filtro');

  @override
  String get filtro {
    _$filtroAtom.reportRead();
    return super.filtro;
  }

  @override
  set filtro(String value) {
    _$filtroAtom.reportWrite(value, super.filtro, () {
      super.filtro = value;
    });
  }

  final _$paroquiaAtivaAtom =
      Atom(name: '_ParoquiasControllerBase.paroquiaAtiva');

  @override
  Igreja get paroquiaAtiva {
    _$paroquiaAtivaAtom.reportRead();
    return super.paroquiaAtiva;
  }

  @override
  set paroquiaAtiva(Igreja value) {
    _$paroquiaAtivaAtom.reportWrite(value, super.paroquiaAtiva, () {
      super.paroquiaAtiva = value;
    });
  }

  final _$setParoquiaAtivaAsyncAction =
      AsyncAction('_ParoquiasControllerBase.setParoquiaAtiva');

  @override
  Future setParoquiaAtiva(DocumentSnapshot<Object> doc) {
    return _$setParoquiaAtivaAsyncAction.run(() => super.setParoquiaAtiva(doc));
  }

  final _$getParoquiasAsyncAction =
      AsyncAction('_ParoquiasControllerBase.getParoquias');

  @override
  Future<List<Igreja>> getParoquias() {
    return _$getParoquiasAsyncAction.run(() => super.getParoquias());
  }

  final _$atualizarLocationAsyncAction =
      AsyncAction('_ParoquiasControllerBase.atualizarLocation');

  @override
  Future atualizarLocation() {
    return _$atualizarLocationAsyncAction.run(() => super.atualizarLocation());
  }

  final _$_ParoquiasControllerBaseActionController =
      ActionController(name: '_ParoquiasControllerBase');

  @override
  dynamic setFiltro(String value) {
    final _$actionInfo = _$_ParoquiasControllerBaseActionController.startAction(
        name: '_ParoquiasControllerBase.setFiltro');
    try {
      return super.setFiltro(value);
    } finally {
      _$_ParoquiasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
lista: ${lista},
totalParoquias: ${totalParoquias},
contParoquia: ${contParoquia},
filtro: ${filtro},
paroquiaAtiva: ${paroquiaAtiva}
    ''';
  }
}
