import 'package:mobx/mobx.dart';

part 'capelas_controller.g.dart';

class CapelasController = _CapelasControllerBase with _$CapelasController;

abstract class _CapelasControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
