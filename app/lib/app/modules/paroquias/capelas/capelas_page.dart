import 'package:flutter/material.dart';
// import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class CapelasPage extends StatefulWidget {
  final String title;
  const CapelasPage({Key key, this.title = "Capelas", List doc})
      : super(key: key);

  @override
  _CapelasPageState createState() => _CapelasPageState();
}

class _CapelasPageState extends State<CapelasPage> {
  UtilsController util = Modular.get();
  ParoquiasController paroquiasController = Modular.get();
  DocumentsController documentsController = new DocumentsController();

  @override
  Widget build(BuildContext context) {
    List doc = paroquiasController.paroquiaAtiva.capelas;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child:
            // Column(
            //   children: <Widget>[
            // Observer(
            //   builder: (_) => util.textoEsquerda(
            //       paroquiasController.paroquiaAtiva.nome,
            //       stilo: util.stHeader),
            // ),
            ListView.builder(
          shrinkWrap: true,
          //   scrollDirection: Axis.horizontal,
          itemCount: doc.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              elevation: 10,
              child: Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      readOnly: true,
                      initialValue: doc[index].nome,
                      decoration: InputDecoration(labelText: 'Capela'),
                    ),
                    TextFormField(
                      readOnly: true,
                      initialValue: doc[index].endereco + ' ',
                      decoration: InputDecoration(labelText: 'Endereço'),
                    ),
                    TextFormField(
                      readOnly: true,
                      initialValue: doc[index].telefone,
                      decoration: InputDecoration(labelText: 'Telefones'),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
        //  ],
        //  ),
      ),
    );
  }
}
