import 'package:santabarbara/app/modules/paroquias/capelas/capelas_controller.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_page.dart';

class ParoquiasModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => CapelasController()),
        Bind((i) => ParoquiasController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => ParoquiasPage()),
      ];

  static Inject get to => Inject<ParoquiasModule>(); //>.of(;
}
