// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'documents_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DocumentsController on _DocumentsControllerBase, Store {
  final _$documentsAtom = Atom(name: '_DocumentsControllerBase.documents');

  @override
  DocumentsModel get documents {
    _$documentsAtom.reportRead();
    return super.documents;
  }

  @override
  set documents(DocumentsModel value) {
    _$documentsAtom.reportWrite(value, super.documents, () {
      super.documents = value;
    });
  }

  final _$listaPastoraisAtom =
      Atom(name: '_DocumentsControllerBase.listaPastorais');

  @override
  List<dynamic> get listaPastorais {
    _$listaPastoraisAtom.reportRead();
    return super.listaPastorais;
  }

  @override
  set listaPastorais(List<dynamic> value) {
    _$listaPastoraisAtom.reportWrite(value, super.listaPastorais, () {
      super.listaPastorais = value;
    });
  }

  final _$filterPastoraisAtom =
      Atom(name: '_DocumentsControllerBase.filterPastorais');

  @override
  String get filterPastorais {
    _$filterPastoraisAtom.reportRead();
    return super.filterPastorais;
  }

  @override
  set filterPastorais(String value) {
    _$filterPastoraisAtom.reportWrite(value, super.filterPastorais, () {
      super.filterPastorais = value;
    });
  }

  final _$filterArtigosAtom =
      Atom(name: '_DocumentsControllerBase.filterArtigos');

  @override
  String get filterArtigos {
    _$filterArtigosAtom.reportRead();
    return super.filterArtigos;
  }

  @override
  set filterArtigos(String value) {
    _$filterArtigosAtom.reportWrite(value, super.filterArtigos, () {
      super.filterArtigos = value;
    });
  }

  final _$questionsListAtom =
      Atom(name: '_DocumentsControllerBase.questionsList');

  @override
  List<dynamic> get questionsList {
    _$questionsListAtom.reportRead();
    return super.questionsList;
  }

  @override
  set questionsList(List<dynamic> value) {
    _$questionsListAtom.reportWrite(value, super.questionsList, () {
      super.questionsList = value;
    });
  }

  final _$getPastoraisAsyncAction =
      AsyncAction('_DocumentsControllerBase.getPastorais');

  @override
  Future<List<dynamic>> getPastorais() {
    return _$getPastoraisAsyncAction.run(() => super.getPastorais());
  }

  @override
  String toString() {
    return '''
documents: ${documents},
listaPastorais: ${listaPastorais},
filterPastorais: ${filterPastorais},
filterArtigos: ${filterArtigos},
questionsList: ${questionsList}
    ''';
  }
}
