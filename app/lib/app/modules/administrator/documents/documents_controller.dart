import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documents/repository/DocumentsRepository.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

part 'documents_controller.g.dart';

class DocumentsController = _DocumentsControllerBase with _$DocumentsController;

abstract class _DocumentsControllerBase with Store {
  UtilsController util = Modular.get();
  @observable
  DocumentsModel documents;

  @observable
  List listaPastorais = [];

  @observable
  String filterPastorais = '';

  @observable
  String filterArtigos = '';

  @observable
  List questionsList = [];

  set setDocuments(DocumentsModel value) => documents = value;
  DocumentsModel get get => documents;

  DocumentsRepository documentsRepository = new DocumentsRepository();
  Stream<QuerySnapshot> getStreamList(Category categoria) {
    return documentsRepository.xGetStream();
  }

  Stream<QuerySnapshot> xGetStreamList(Category categoria) {
    return documentsRepository.xGetStream();
  }

  Stream<QuerySnapshot> getStreamAvisos() {
    Stream<QuerySnapshot> snap = FirebaseFirestore.instance
        .collection('artigos')
        .where('ativo', isEqualTo: true)
        .snapshots();
    return snap;
  }

  Stream<QuerySnapshot> getStreamByGroup(String group) {
    var query = FirebaseFirestore.instance.collection('artigos');
    if (filterArtigos != '') {
      query = query.where('titulo', isGreaterThanOrEqualTo: filterPastorais);
    }
    return query
        .where('grupo', isEqualTo: group) //.toString())
        .where('ativo', isEqualTo: true)
        .snapshots();
    // return snap;
  }

  Future<QuerySnapshot> getByGroup(int group) {
    Future<QuerySnapshot> doc = FirebaseFirestore.instance
        .collection('artigos')
        .where('grupo', isEqualTo: group.toString())
        .where('ativo', isEqualTo: true)
        .get();

    doc.then((d) => {
          print(d),
          documents = new DocumentsModel(),
          documents.id = d.docs[0].get('id'),
          documents.titulo = d.docs[0].get('titulo'),
          documents.subtitulo = d.docs[0].get('subtitulo'),
          documents.conteudo = d.docs[0].get('conteudo'),
          documents.imagem = d.docs[0].get('imagem'),
        });
    return doc;
  }

  @action
  Future<List> getPastorais() async {
    var snap = FirebaseFirestore.instance
        .collection('pastorais')
        .orderBy("titulo")
        .snapshots();
    listaPastorais = [];
    snap.forEach((field) {
      field.docs.asMap().forEach((index, data) {
        // print(data);
        try {
          if (dadoValidoPesquisa(data['titulo']) |
              dadoValidoPesquisa(data['sigla'])) {
            listaPastorais.add(data);
          }
        } catch (e) {
          print('error $e');
        }
      });
    });
    // print(listaPastorais);
    return listaPastorais;
  }

  bool dadoValidoPesquisa(value) {
    return (value
                .toString()
                .toLowerCase()
                .indexOf(filterPastorais.toLowerCase()) !=
            -1) |
        (filterPastorais == '');
  }

  //@action
  getQuestions() async {
    print('GETTING QUESTIONS');
    Future<QuerySnapshot> snap = FirebaseFirestore.instance
        .collection('artigos')
        //   .where('grupo', isEqualTo: '700')
        //   .where('ativo', isEqualTo: true)
        .get();
    //   .then((onValue) => print('ONVALUE $onValue'));
    int i = 0;
    try {
      await snap.then(
        (doc) => {
          questionsList = [],
          for (DocumentSnapshot item in doc.docs)
            {
              if (item.get('subtitulo') != null)
                {
                  questionsList.add(
                    {
                      '\'name\'': '\'${item.get('titulo')}\'',
                      '\'group\'': '\'${item.get('subtitulo')}\''
                    },
                  ),
                  print(i),
                  i++,
                }
            },
        },
      );
    } catch (e) {
      print('error $e');
    }
    questionsList = [
      {'name': 'Retornoi', 'group': 'Team A'},
      {'name': 'Will', 'group': 'Team B'},
      {'name': 'Beth', 'group': 'Team A'},
      {'name': 'Miranda', 'group': 'Team B'},
      {'name': 'dfgdfgdfgdgdfgdfgdgdf', 'group': 'Team C'},
      {'name': 'Danny', 'group': 'Team C'},
    ];
  }
}
