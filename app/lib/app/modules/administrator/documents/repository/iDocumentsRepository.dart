import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

abstract class IDocumentsRepository {
  // Future<List<DocumentsModel>> getList();
  Stream<QuerySnapshot> getStream(Category categoria);
  Stream<QuerySnapshot> getStreamAvisos();
  Stream<QuerySnapshot> xGetStream();
  Future<List<DocumentsModel>> getFiltered(DocumentsModel value);
  Future<List<DocumentsModel>> getById(String id);
  Future<List<DocumentsModel>> add(DocumentsModel value);
  Future<List<DocumentsModel>> upd(DocumentsModel value);
  Future<List<DocumentsModel>> del(String id);
  // teste
}
