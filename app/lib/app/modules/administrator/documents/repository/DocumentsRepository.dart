import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documents/repository/iDocumentsRepository.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class DocumentsRepository implements IDocumentsRepository {
  UtilsController util = Modular.get();
  @override
  Future<List<DocumentsModel>> add(DocumentsModel value) {
    throw UnimplementedError();
  }

  @override
  Future<List<DocumentsModel>> del(String id) {
    throw UnimplementedError();
  }

  @override
  Future<List<DocumentsModel>> getById(String id) {
    throw UnimplementedError();
  }

  @override
  Future<List<DocumentsModel>> getFiltered(DocumentsModel value) {
    throw UnimplementedError();
  }

  @override
  Stream<QuerySnapshot> getStream(Category categoria) {
    return FirebaseFirestore.instance
        .collection('artigos')
        .where('categoria', isEqualTo: categoria.codigo)
        .where('ativo', isEqualTo: true)
        .snapshots();
  }

  @override
  Stream<QuerySnapshot> xGetStream() {
    return FirebaseFirestore.instance
        .collection('artigos')
        .where('categoria', isEqualTo: 'avisos')
        .where('ativo', isEqualTo: true)
        .snapshots();
  }

  @override
  Future<List<DocumentsModel>> upd(DocumentsModel value) {
    throw UnimplementedError();
  }

  // @override
  // Future<List<DocumentsModel>> getList() {
  //   Future<List<DocumentsModel>> result;
  //   Stream<QuerySnapshot> lista = Firestore.instance
  //       .collection('artigos')
  //       .where('categoria', isEqualTo: 'avisos')
  //       .where('ativo', isEqualTo: true)
  //       .snapshots();

  //   lista.forEach((element) {
  //     // element.docs[0].
  //   });
  //   //throw UnimplementedError();
  // }

  @override
  Stream<QuerySnapshot> getStreamAvisos() {
    //return getStream(util.categoryAvisos());
    return FirebaseFirestore.instance
        .collection('artigos')
        .where('categoria', isEqualTo: 'aviso')
        .where('ativo', isEqualTo: true)
        .snapshots();
  }
}
