import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'oracao_template_controller.dart';

class OracaoTemplatePage extends StatefulWidget {
  final String title;
  const OracaoTemplatePage({Key key, this.title = "OracaoTemplate"})
      : super(key: key);

  @override
  _OracaoTemplatePageState createState() => _OracaoTemplatePageState();
}

class _OracaoTemplatePageState
    extends ModularState<OracaoTemplatePage, OracaoTemplateController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
