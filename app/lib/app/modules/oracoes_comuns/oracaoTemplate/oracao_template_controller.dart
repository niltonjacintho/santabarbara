import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'oracao_template_controller.g.dart';

@Injectable()
class OracaoTemplateController = _OracaoTemplateControllerBase
    with _$OracaoTemplateController;

abstract class _OracaoTemplateControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
