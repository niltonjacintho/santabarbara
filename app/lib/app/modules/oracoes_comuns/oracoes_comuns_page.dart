import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/oracoes_comuns/oracoes_comuns_controller.dart';
import 'package:santabarbara/app/store/base_store.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:santabarbara/app/models/oracao_model.dart';

class OracoesComunsPage extends StatefulWidget {
  final String title;
  const OracoesComunsPage({Key key, this.title = "OracoesComuns"})
      : super(key: key);

  @override
  _OracoesComunsPageState createState() => _OracoesComunsPageState();
}

class _OracoesComunsPageState
    extends ModularState<OracoesComunsPage, OracoesComunsController> {
  //use 'controller' variable to access controller
// Stream<QuerySnapshot> myStream =
//        oracoesComunsController.getOracoesf('');

  UtilsController util = Modular.get();
  OracoesComunsController oracoesComunsController = Modular.get();
  BaseStore baseStore = Modular.get();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: util.corFundoAvisos,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text('Orações'),
              backgroundColor: Colors.red[900],
              expandedHeight: 200,
              floating: true,
              pinned: true,
              snap: true,
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.refresh),
                  tooltip: 'Atualizar orações',
                  onPressed: () {
                    // oracoesComunsController.getOracoes(
                    //     '', true); // handle the press
                  },
                ),
              ],
              flexibleSpace: FlexibleSpaceBar(
                background: Image.asset(
                  'assets/icon/rezando.png',
                  fit: BoxFit.cover,
                ),
                //Text('2222'),
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: oracoesComunsController.getOracoes(''),
              builder: (context, snapshot) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                onTap: () {
                                  baseStore.oracao = new Oracoes(
                                      titulo: snapshot.data.docs[index]
                                          .get('titulo'),
                                      id: snapshot.data.docs[index]
                                          .get('id'),
                                      texto: snapshot.data.docs[index]
                                          .get('texto'));
                                  Modular.to.pushNamed('/oracaoView');
                                },
                                title: Text(
                                  snapshot.data.docs[index].get('titulo'),
                                  style: TextStyle(fontSize: 24),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    childCount:
                        snapshot.hasData ? snapshot.data.docs.length : 0,
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
