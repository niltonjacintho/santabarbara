import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:get_storage/get_storage.dart';

part 'oracoes_comuns_controller.g.dart';

@Injectable()
class OracoesComunsController = _OracoesComunsControllerBase
    with _$OracoesComunsController;

abstract class _OracoesComunsControllerBase with Store {
  @observable
  int value = 0;

  @observable
  List listaOracoes = new List();

  final box = GetStorage();

  @action
  void increment() {
    value++;
  }

  @action
  Future getListaOracoesFromStore(String grupo) async {
    // var docs = FirebaseFirestore.instance.collection("oracoes").get();
    grupo = grupo == '' ? 'comuns' : grupo;

    // for (var item in docs['grupo']) {
    //   if (item['nome'].toLowerCase() == 'comuns'.toLowerCase()) {
    //     for (var o in item['oracoes']) {
    //       // print(o);
    //       listaOracoes.add(o);
    //     }
    //   }
    // }
    return listaOracoes;
  }

  @action
  Stream<QuerySnapshot> getOracoes(String grupo) {
    var query = FirebaseFirestore.instance.collection('oracoes');
    // if (filterArtigos != '') {
    //   query = query.where('titulo', isGreaterThanOrEqualTo: filterPastorais);
    // }
    return query.where('grupo', isEqualTo: 'comum').snapshots();
    // .where('ativo', isEqualTo: true)
    // .snapshots();
    // return snap;
  }

  updateOracoes() {
    var doc = FirebaseFirestore.instance.collection("oracoes").get();
    //loadKeys(x.length);
    doc.then((d) =>
        {print(d.docs[0].data()), box.write('oracao', d.docs[0].data())});
    return doc;
  }
}
