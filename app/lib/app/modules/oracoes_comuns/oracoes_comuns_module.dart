import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/oracoes_comuns/oracoes_comuns_page.dart';
import 'package:santabarbara/app/modules/oracoes_comuns/oracoes_comuns_controller.dart';

class OracoesComunsModule extends Module {
  @override
  List<Bind> get binds => [
        // $OracaoTemplateController,
        Bind((i) => OracoesComunsController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => OracoesComunsPage()),
      ];

  static Inject get to => Inject<OracoesComunsModule>(); //>.of(;
}
