// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oracoes_comuns_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$OracoesComunsController on _OracoesComunsControllerBase, Store {
  final _$valueAtom = Atom(name: '_OracoesComunsControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$listaOracoesAtom =
      Atom(name: '_OracoesComunsControllerBase.listaOracoes');

  @override
  List<dynamic> get listaOracoes {
    _$listaOracoesAtom.reportRead();
    return super.listaOracoes;
  }

  @override
  set listaOracoes(List<dynamic> value) {
    _$listaOracoesAtom.reportWrite(value, super.listaOracoes, () {
      super.listaOracoes = value;
    });
  }

  final _$getListaOracoesFromStoreAsyncAction =
      AsyncAction('_OracoesComunsControllerBase.getListaOracoesFromStore');

  @override
  Future<dynamic> getListaOracoesFromStore(String grupo) {
    return _$getListaOracoesFromStoreAsyncAction
        .run(() => super.getListaOracoesFromStore(grupo));
  }

  final _$_OracoesComunsControllerBaseActionController =
      ActionController(name: '_OracoesComunsControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_OracoesComunsControllerBaseActionController
        .startAction(name: '_OracoesComunsControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_OracoesComunsControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Stream<QuerySnapshot<Object>> getOracoes(String grupo) {
    final _$actionInfo = _$_OracoesComunsControllerBaseActionController
        .startAction(name: '_OracoesComunsControllerBase.getOracoes');
    try {
      return super.getOracoes(grupo);
    } finally {
      _$_OracoesComunsControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
listaOracoes: ${listaOracoes}
    ''';
  }
}
