import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_controller.dart';

class MapPage extends StatefulWidget {
  final String title;
  final double lat;
  final double long;
  const MapPage({Key key, this.title = "Map", this.lat = 0.0, this.long = 0.0})
      : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  GoogleMapController mapController;
  ParoquiasController paroquiasController = Modular.get();
  Set<Marker> markers = new Set<Marker>();
  _onMapCreated(GoogleMapController controler) {
    mapController = controler;
    final Marker marker = Marker(
      markerId: MarkerId("1"),
      position: LatLng(paroquiasController.paroquiaAtiva.lat,
          paroquiasController.paroquiaAtiva.long),
      infoWindow: InfoWindow(
        title: paroquiasController.paroquiaAtiva.nome,
        snippet: paroquiasController.paroquiaAtiva.paroco,
      ),
    );
    setState(() {
      if (paroquiasController.paroquiaAtiva.long != null) {
        markers.add(marker);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: GoogleMap(
        onMapCreated: _onMapCreated,
        mapToolbarEnabled: true,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        initialCameraPosition: CameraPosition(
            target: LatLng(paroquiasController.paroquiaAtiva.lat,
                paroquiasController.paroquiaAtiva.long),
            zoom: 13.5),
        markers: markers,
      ),
    );
  }
}
