import 'package:santabarbara/app/modules/map/map_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/map/map_page.dart';

class MapModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => MapController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => MapPage()),
      ];

  static Inject get to => Inject<MapModule>(); //>.of(;
}
