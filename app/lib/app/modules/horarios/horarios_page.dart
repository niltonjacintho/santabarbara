import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:material_dialog/material_dialog.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'horarios_controller.dart';

class HorariosPage extends StatefulWidget {
  final String title;
  const HorariosPage({Key key, this.title = "Horarios"}) : super(key: key);

  @override
  _HorariosPageState createState() => _HorariosPageState();
}

class _HorariosPageState
    extends ModularState<HorariosPage, HorariosController> {
  //use 'controller' variable to access controller
  UtilsController util = Modular.get();
  DocumentsController documentsController = Modular.get();
  @override
  Widget build(BuildContext context) {
    documentsController.getByGroup(util.grupoHorarios.value);
    return MaterialDialog(
      enableCloseButton: true,
      enableFullWidth: true,
      enableFullHeight: true,
      onCloseButtonClicked: () {
        Navigator.pop(context);
      },
      title: Column(
        children: <Widget>[
          Observer(
            builder: (_) => Text(documentsController.documents.titulo),
          ),
          SizedBox(
            height: 3,
          ),
          Divider(
            height: 3,
            thickness: 2,
            color: Colors.lightBlue,
          ),
          //    );},
        ],
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Observer(
              builder: (_) => Text(documentsController.documents.conteudo),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Voltar'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
