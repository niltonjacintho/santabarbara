import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/modules/horarios/horarios_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/horarios/horarios_page.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class HorariosModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => HorariosController()),
        Bind((i) => UtilsController()),
        Bind((i) => DocumentsController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => HorariosPage()),
      ];

  static Inject get to => Inject<HorariosModule>(); //>.of(;
}
