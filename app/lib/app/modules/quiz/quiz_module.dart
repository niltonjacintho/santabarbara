import 'quizTop/quiz_top_controller.dart';
import 'quizFinal/quiz_final_controller.dart';
import 'quizGame/quiz_game_controller.dart';
import 'package:santabarbara/app/modules/quiz/quiz_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/quiz/quiz_page.dart';

class QuizModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => QuizTopController()),
        Bind((i) => QuizFinalController()),
        Bind((i) => QuizGameController()),
        Bind((i) => QuizController()),
        Bind((i) => QuizTopController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => QuizPage()),
      ];

  static Inject get to => Inject<QuizModule>(); //>.of(;
}
