import 'package:flutter/material.dart';
import 'package:flutter_circular_text/circular_text/model.dart';
import 'package:flutter_circular_text/circular_text/widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:google_fonts/google_fonts.dart';
import 'quiz_controller.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';

class QuizPage extends StatefulWidget {
  final String title;
  const QuizPage({Key key, this.title = "Quiz"}) : super(key: key);

  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends ModularState<QuizPage, QuizController> {
  final List<Color> colors = [
    Colors.red,
    Colors.orange,
    Colors.yellow,
    Colors.green,
    Colors.blue,
    Colors.indigo,
    Colors.purple,
  ];
  //bool _isPlaying = false;
 // RandomColor _randomColor = RandomColor();
  final _sliderKey = GlobalKey();

  //use 'controller' variable to access controller
  QuizController quizController = Modular.get();
  @override
  Widget build(BuildContext context) {
    quizController.getTopicos();
    return Scaffold(
      backgroundColor: UtilsController().corFundoPadrao,
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Align(
              alignment: Alignment.topCenter,
              child: CircularText(
                children: [
                  TextItem(
                    text: Text(
                      "Quiz Católico".toUpperCase(),
                      style: TextStyle(
                        fontSize: 28,
                        color: Colors.red[900],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    space: 12,
                    startAngle: -90,
                    startAngleAlignment: StartAngleAlignment.center,
                    direction: CircularTextDirection.clockwise,
                  ),
                  TextItem(
                    text: Text(
                      "Você Consegue?".toUpperCase(),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.amberAccent[400],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    space: 10,
                    startAngle: 90,
                    startAngleAlignment: StartAngleAlignment.center,
                    direction: CircularTextDirection.anticlockwise,
                  ),
                ],
                radius: 125,
                position: CircularTextPosition.inside,
                backgroundPaint: Paint()..color = Colors.yellow[50],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Card(
              elevation: 50,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Escolha um tema! Vamos lá',
                  style:
                      GoogleFonts.sriracha(color: Colors.black, fontSize: 25),
                ),
              ),
            ),
            Container(
              height: 200,
              child: Padding(
                padding: EdgeInsets.all(30),
                child: Observer(
                  builder: (_) => quizController.listaTopicos.length > 0
                      ? CarouselSlider.builder(
                          key: _sliderKey,
                          unlimitedMode: true,
                          slideBuilder: (index) {
                            return Container(
                              alignment: Alignment.center,
                              color: quizController.listaTopicos[index].cor,
                              child: GestureDetector(
                                onTap: () {
                                  print(quizController.listaTopicos[index]);
                                },
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: 40),
                                  child: Text(
                                    quizController.getTopicoById(index).nome,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 30, color: Colors.white),
                                  ),
                                ),
                              ),
                            );
                          },
                          slideTransform: CubeTransform(),
                          slideIndicator: CircularSlideIndicator(
                            padding: EdgeInsets.only(bottom: 32),
                          ),
                          itemCount: colors.length,
                        )
                      : Container(),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Observer(
              builder: (_) => RaisedButton.icon(
                onPressed: () async {
                  print(quizController.topicoAtual);
                  await quizController.criarQuiz(quizController.topicoAtual);
                  Modular.to.pushNamed('/quizpage');
                },
                icon: Icon(Icons.arrow_right),
                label: Text(
                  'Vamos!!!',
                  style: new TextStyle(fontSize: 20),
                ),
              ),
            ),
            //  ),
          ],
        ),
      ),
    );
  }
}
