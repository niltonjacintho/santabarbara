import 'package:mobx/mobx.dart';

part 'quiz_top_controller.g.dart';

class QuizTopController = _QuizTopControllerBase with _$QuizTopController;

abstract class _QuizTopControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
