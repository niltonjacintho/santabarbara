import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:random_color/random_color.dart';
import 'package:santabarbara/app/models/quizRank_model.dart';
import 'package:santabarbara/app/models/quiz_model.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:http/http.dart' as http;
import 'package:santabarbara/app/services/db_service.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

part 'quiz_controller.g.dart';

class QuizController = _QuizControllerBase with _$QuizController;

abstract class _QuizControllerBase with Store {
  var db = FirebaseFirestore.instance;
  AuthController authController = Modular.get();
  DbService dbService = Modular.get();

  @observable
  List<BasePerguntas> perguntas = new List<BasePerguntas>();

  @observable
  BaseTopicos topicoAtual = new BaseTopicos();

  @observable
  List<BaseTopicos> listaTopicos = new List<BaseTopicos>();

  @observable
  BasePerguntas basePerguntas = new BasePerguntas();

  @observable
  List<BasePerguntas> listaPerguntas = new List<BasePerguntas>();

  @observable
  int perguntaAtual = 1;

  @action
  incPerguntaAtual() {
    perguntaAtual++;
  }

  @action
  resetPerguntaAtual() {
    perguntaAtual = 1;
  }

  @observable
  Desafios desafio = new Desafios();

  @observable
  int qtdAcertos = 0;

  @observable
  List<QuizRankModel> listaTop = new List<QuizRankModel>();

  @action
  marcarAcerto() {
    qtdAcertos++;
  }

  int qtdPerguntas = 10;
  final int tempoPadrao = 180;
  int _contPerguntas = 0;
  @observable
  int tempoRestante = 0;

  @observable
  bool stopCount = false;

  @action
  initQuiz({int tempo}) {
    tempoRestante = tempo == null ? tempoPadrao : tempo;
    stopCount = false;
    _contPerguntas = 0;
    qtdAcertos = 0;
    stopCount = false;
    perguntaAtual = 1;
  }

  @action
  decTempoRestante() {
    if (tempoRestante > 0) {
      tempoRestante--;
    }
  }

  double get pontuacaoFinal {
    return qtdAcertos * (tempoRestante / 10);
  }

  @action
  void startCountdown() {
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (stopCount) {
        timer.cancel();
        tempoRestante = tempoPadrao;
      } else {
        decTempoRestante();
      }
      // print(tempoRestante);
    });
  }

  salvarResultado() {
    QuizRankModel q = new QuizRankModel();
    q.pontos = pontuacaoFinal;
    q.acertos = qtdAcertos;
    q.erros = qtdPerguntas - qtdAcertos;
    q.data = DateTime.now();
    q.nome = authController.user.displayName;
    q.email = authController.user.providerData[0].email;
    q.topico = topicoAtual.nome;
    dbService.salvarObjeto(q, 'quizrank');
  }

  @action
  void stopCountdown() {
    stopCount = true;
  }

  int get getTempoRestante {
    return tempoRestante;
  }

  int get getTempoCorrido {
    return tempoPadrao - tempoRestante;
  }

  String getFormatTempoCorrido() {
    Duration d = Duration(hours: 0, minutes: 0, seconds: getTempoCorrido);
    return d.toString().split('.').first.padLeft(8, "0").substring(3);
  }

  String getFormatTempoRestante() {
    Duration d = Duration(hours: 0, minutes: 0, seconds: tempoRestante);
    return d.toString().split('.').first.padLeft(8, "0").substring(3);
  }

  UtilsController _utilsController = Modular.get();
  AuthController _authController = Modular.get();

  Future<bool> limparBase() async {
    await DbService().cleanFolder('quiztopicos');
    await DbService().cleanFolder('quizperguntas');
    return true;
  }

  @action
  Future<List<BaseTopicos>> getTopicos({qtd = 999}) async {
    RandomColor _randomColor = new RandomColor();
    await db.collection('quiztopicos').get().then(
      (querySnapshot) {
        listaTopicos = new List<BaseTopicos>();
        querySnapshot.docs.forEach((doc) {
          listaTopicos.add(new BaseTopicos(
              nome: doc.data()['nome'],
              id: doc.data()['id'],
              cor: _randomColor.randomColor()));
        });
      },
    );
    return listaTopicos;
  }

  @action
  BaseTopicos getTopicoById(int index) {
    topicoAtual = listaTopicos[index];
    return topicoAtual;
  }

  initialData(q) {
    var x = new List();
    q.nome = '';
    q.pontos = 0.00;
    q.email = '';
    q.acertos = 0;
    q.erros = 0;
    q.id = '';
    x.add(q);
    x.add(q);
    x.add(q);
    return x;
  }

  @action
  Future<List<QuizRankModel>> getTopN({int n = 3}) async {
    listaTop = new List<QuizRankModel>();
    await db
        .collection('quizrank')
        .orderBy('pontos', descending: true)
        .limit(n)
        .get()
        .then((querySnapshot) {
      QuizRankModel q = new QuizRankModel();
      var element;
      for (int i = 0; i <= querySnapshot.docs.length - 1; i++) {
        element = querySnapshot.docs[i];
        //  querySnapshot.docs.forEach((var element) async {
        q = new QuizRankModel();
        q.nome = element.data()['nome'];
        q.pontos = element.data()['pontos'];
        q.email = element.data()['email'];
        q.acertos = element.data()['acertos'];
        q.erros = element.data()['erros'];
        // q.data = element.data()['data'];
        //  q.topico = element.data()['topico'];
        q.id = element.data()['id'];
        listaTop.add(q);
      }
    });
    return listaTop;
  }

  @action
  Future<List<QuizRankModel>> getTopNOffLine() async {
    listaTop = new List<QuizRankModel>();
    QuizRankModel q;
    for (int i = 0; i < 4; i++) {
      q = new QuizRankModel();
      q.nome = 'Nome Cesar Jacintho$i ';
      q.pontos = Random.secure().nextDouble();
      q.email = 'Nome$i@gmail.com';
      q.acertos = Random.secure().nextInt(50);
      q.erros = Random.secure().nextInt(30);
      q.topico = 'Arca de Noé';
      q.data = DateTime.now();
      listaTop.add(q);
    }
    return listaTop;
  }

  @action
  Future<List<BasePerguntas>> getPerguntasPorTopicos(BaseTopicos topico) async {
    // Random random = new Random();
    int randomNumber;
    var query =
        db.collection('quizperguntas').where('idTopico', isEqualTo: topico.id);
    query.get().then((querySnapshot) {
      int i = 0;
      PerguntasRespostas r = new PerguntasRespostas();
      BasePerguntas p = new BasePerguntas();
      listaPerguntas = new List<BasePerguntas>();
      while (i <= qtdPerguntas) {
        randomNumber = _utilsController.gerarRnd(querySnapshot.docs.length);
        var doc = querySnapshot.docs[randomNumber];
        p = new BasePerguntas();
        p.id = doc.data()['id'];
        p.perguntasRespostas = new List<PerguntasRespostas>();
        p.pergunta = doc.data()['pergunta'];
        p.idTopico = doc.data()['idTopico'];
        List X = doc.data()['perguntasRespostas'];
        X.forEach((item) {
          r = new PerguntasRespostas();
          r.sequencia = item['sequencia'];
          r.respostacerta = item['respostacerta'];
          r.respostatexto = item['respostatexto'];
          r.id = item['id'];
          p.perguntasRespostas.add(r);
        });
        listaPerguntas.add(p);
        i++;
      }
    });
    return listaPerguntas;
  }

  @action
  carregarTopicos() async {
    listaUrls.forEach((element) {
      BaseTopicos b = new BaseTopicos(
          nome: element.id,
          cor: null,
          id: element.id.trim().replaceAll(' ', '').toLowerCase());
      //   listaTopicos.add(b);
      DbService().salvarObjeto(b, 'quiztopicos');
    });
  }

  List<BaseTopicos> listaUrls = new List<BaseTopicos>();

  inicializarUrls() {
    RandomColor _randomColor = new RandomColor();
    listaUrls = new List<BaseTopicos>();
    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/perguntas-biblicas-faceis/',
        id: 'Biblia nivel fácil',
        cor: _randomColor.randomColor()));
    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/50-perguntas-biblicas-nivel-medio/',
        id: 'Biblia nivel médio',
        cor: _randomColor.randomColor()));

    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/perguntas-biblicas-infantil/',
        id: 'Biblia nivel infantil',
        cor: _randomColor.randomColor()));

    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/perguntas-biblicas-dificil/',
        id: 'Biblia nivel difícil',
        cor: _randomColor.randomColor()));

    listaUrls.add(new BaseTopicos(
        nome:
            'https://www.respostas.com.br/perguntas-biblicas-noe-arca-diluvio/',
        id: 'Arca de Noé',
        cor: _randomColor.randomColor()));

    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/perguntas-biblicas-jesus-cristo/',
        id: 'Jesus Cristo',
        cor: _randomColor.randomColor()));

    listaUrls.add(new BaseTopicos(
        nome: 'https://www.respostas.com.br/perguntas-biblicas-natal/',
        id: 'Natal',
        cor: _randomColor.randomColor()));
    listaUrls.add(new BaseTopicos(
        nome:
            'https://www.respostas.com.br/perguntas-biblicas-velho-testamento/',
        id: 'Velho Testamento',
        cor: _randomColor.randomColor()));
  }

  @action
  carregarPerguntas() async {
    //await limparBase;
    await inicializarUrls();
    await carregarTopicos();
    _contPerguntas = 0;
    listaPerguntas = new List<BasePerguntas>();
    for (var element in listaUrls) {
      var response = await http.get(Uri.parse(element.nome));
      _carregarPerguntas(element, response.body);
    }
  }

  @action
  Future<String> _carregarPerguntas(BaseTopicos topico, String html) async {
    var unescape = new HtmlUnescape();
    try {
      html = unescape.convert(html);
      html =
          html.substring(html.toLowerCase().indexOf('<article'.toLowerCase()));
      BasePerguntas p = new BasePerguntas();
      while (html.toLowerCase().indexOf('ver resposta'.toLowerCase()) >= 0) {
        //pega a perguntas
        p = new BasePerguntas();
        html = html.substring(html.indexOf('<h3>'));
        p.pergunta = html.substring(4, html.indexOf('</h3'));
        try {
          p.pergunta = p.pergunta
              .substring(p.pergunta.indexOf(new RegExp(r'[A-Z][a-z]')))
              .trim();
        } catch (e) {
          print(null);
        }
        if (p.pergunta.indexOf('Qual o nome do monte onde') != -1)
          print('parar aqui');
        p.idTopico = topico.id.trim().replaceAll(' ', '').toLowerCase();
        html = html.substring(html.indexOf('</h3') + 5);
        // Respostas
        List<String> tempRespostas = html
            .substring(0, html.indexOf('<div class'))
            .replaceAll('<p>', '')
            .replaceAll('</p>', ';')
            .split(';');
        //limpa o lixo dos textos
        List<String> temp = new List<String>();
        tempRespostas.forEach((element) {
          if (element != "") {
            if (element.indexOf(') ') > 0)
              temp.add(element.substring(element.indexOf(') ') - 1));
            else
              temp.add(element);
          }
        });
        tempRespostas = temp;
        print(tempRespostas); //
        //pergar resposta certa
        String r;
        try {
          r = html.substring(
              html.toLowerCase().indexOf('<div class="hc-pt'.toLowerCase()),
              html.toLowerCase().indexOf('<button>Ver'.toLowerCase()));
          r = r.substring(r.indexOf('<p>'), r.indexOf('</p>'));
          r = r.replaceAll('<p>', '').replaceAll('</p>', '').trim();
          //   print(r);
        } catch (e) {
          //  print(e);
        }
        p.perguntasRespostas = new List<PerguntasRespostas>();
        PerguntasRespostas pr = new PerguntasRespostas();
        int i = 0;
        tempRespostas.forEach((element) {
          if (element != '' && element != '.') {
            pr = new PerguntasRespostas();
            pr.respostatexto = element;
            pr.sequencia = i;
            pr.respostacerta = element.trim().toLowerCase() == r.toLowerCase();
            p.perguntasRespostas.add(pr);
            i++;
          }
        });
        //  listaPerguntas.add(p);
        p.id = _contPerguntas.toString();
        _contPerguntas++;
        DbService().salvarObjeto(p, 'quizperguntas');
        //  print(p);
        try {
          html = html.substring(html.indexOf('<h3>'));
        } catch (e) {
          html = '';
        }
      }
      // print(html);
      return html;
    } catch (e) {
      //  print(e);
      return '';
    }
  }

//#region Metodos da montagem do QUIZ

//#endregion

  @action
  criarQuiz(BaseTopicos topico) async {
    getPerguntasPorTopicos(topico);
    desafio = new Desafios();
    desafio.data = new DateTime.now();
    desafio.finalizado = false;
    desafio.email = _authController.user.providerData[0].email;
    desafio.desafioPerguntas = new List<DesafioPerguntas>();
    if (listaPerguntas.length > 0) {
      listaPerguntas.forEach((element) {
        desafio.desafioPerguntas
            .add(new DesafioPerguntas(id: element.id, acertou: false));
      });
    }
  }
}
