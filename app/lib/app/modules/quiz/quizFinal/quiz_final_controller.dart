import 'package:mobx/mobx.dart';

part 'quiz_final_controller.g.dart';

class QuizFinalController = _QuizFinalControllerBase with _$QuizFinalController;

abstract class _QuizFinalControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
