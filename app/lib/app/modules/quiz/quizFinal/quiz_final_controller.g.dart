// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quiz_final_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QuizFinalController on _QuizFinalControllerBase, Store {
  final _$valueAtom = Atom(name: '_QuizFinalControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$_QuizFinalControllerBaseActionController =
      ActionController(name: '_QuizFinalControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_QuizFinalControllerBaseActionController.startAction(
        name: '_QuizFinalControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_QuizFinalControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value}
    ''';
  }
}
