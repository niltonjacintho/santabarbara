// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quiz_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QuizController on _QuizControllerBase, Store {
  final _$perguntasAtom = Atom(name: '_QuizControllerBase.perguntas');

  @override
  List<BasePerguntas> get perguntas {
    _$perguntasAtom.reportRead();
    return super.perguntas;
  }

  @override
  set perguntas(List<BasePerguntas> value) {
    _$perguntasAtom.reportWrite(value, super.perguntas, () {
      super.perguntas = value;
    });
  }

  final _$topicoAtualAtom = Atom(name: '_QuizControllerBase.topicoAtual');

  @override
  BaseTopicos get topicoAtual {
    _$topicoAtualAtom.reportRead();
    return super.topicoAtual;
  }

  @override
  set topicoAtual(BaseTopicos value) {
    _$topicoAtualAtom.reportWrite(value, super.topicoAtual, () {
      super.topicoAtual = value;
    });
  }

  final _$listaTopicosAtom = Atom(name: '_QuizControllerBase.listaTopicos');

  @override
  List<BaseTopicos> get listaTopicos {
    _$listaTopicosAtom.reportRead();
    return super.listaTopicos;
  }

  @override
  set listaTopicos(List<BaseTopicos> value) {
    _$listaTopicosAtom.reportWrite(value, super.listaTopicos, () {
      super.listaTopicos = value;
    });
  }

  final _$basePerguntasAtom = Atom(name: '_QuizControllerBase.basePerguntas');

  @override
  BasePerguntas get basePerguntas {
    _$basePerguntasAtom.reportRead();
    return super.basePerguntas;
  }

  @override
  set basePerguntas(BasePerguntas value) {
    _$basePerguntasAtom.reportWrite(value, super.basePerguntas, () {
      super.basePerguntas = value;
    });
  }

  final _$listaPerguntasAtom = Atom(name: '_QuizControllerBase.listaPerguntas');

  @override
  List<BasePerguntas> get listaPerguntas {
    _$listaPerguntasAtom.reportRead();
    return super.listaPerguntas;
  }

  @override
  set listaPerguntas(List<BasePerguntas> value) {
    _$listaPerguntasAtom.reportWrite(value, super.listaPerguntas, () {
      super.listaPerguntas = value;
    });
  }

  final _$perguntaAtualAtom = Atom(name: '_QuizControllerBase.perguntaAtual');

  @override
  int get perguntaAtual {
    _$perguntaAtualAtom.reportRead();
    return super.perguntaAtual;
  }

  @override
  set perguntaAtual(int value) {
    _$perguntaAtualAtom.reportWrite(value, super.perguntaAtual, () {
      super.perguntaAtual = value;
    });
  }

  final _$desafioAtom = Atom(name: '_QuizControllerBase.desafio');

  @override
  Desafios get desafio {
    _$desafioAtom.reportRead();
    return super.desafio;
  }

  @override
  set desafio(Desafios value) {
    _$desafioAtom.reportWrite(value, super.desafio, () {
      super.desafio = value;
    });
  }

  final _$qtdAcertosAtom = Atom(name: '_QuizControllerBase.qtdAcertos');

  @override
  int get qtdAcertos {
    _$qtdAcertosAtom.reportRead();
    return super.qtdAcertos;
  }

  @override
  set qtdAcertos(int value) {
    _$qtdAcertosAtom.reportWrite(value, super.qtdAcertos, () {
      super.qtdAcertos = value;
    });
  }

  final _$listaTopAtom = Atom(name: '_QuizControllerBase.listaTop');

  @override
  List<QuizRankModel> get listaTop {
    _$listaTopAtom.reportRead();
    return super.listaTop;
  }

  @override
  set listaTop(List<QuizRankModel> value) {
    _$listaTopAtom.reportWrite(value, super.listaTop, () {
      super.listaTop = value;
    });
  }

  final _$tempoRestanteAtom = Atom(name: '_QuizControllerBase.tempoRestante');

  @override
  int get tempoRestante {
    _$tempoRestanteAtom.reportRead();
    return super.tempoRestante;
  }

  @override
  set tempoRestante(int value) {
    _$tempoRestanteAtom.reportWrite(value, super.tempoRestante, () {
      super.tempoRestante = value;
    });
  }

  final _$stopCountAtom = Atom(name: '_QuizControllerBase.stopCount');

  @override
  bool get stopCount {
    _$stopCountAtom.reportRead();
    return super.stopCount;
  }

  @override
  set stopCount(bool value) {
    _$stopCountAtom.reportWrite(value, super.stopCount, () {
      super.stopCount = value;
    });
  }

  final _$getTopicosAsyncAction = AsyncAction('_QuizControllerBase.getTopicos');

  @override
  Future<List<BaseTopicos>> getTopicos({dynamic qtd = 999}) {
    return _$getTopicosAsyncAction.run(() => super.getTopicos(qtd: qtd));
  }

  final _$getTopNAsyncAction = AsyncAction('_QuizControllerBase.getTopN');

  @override
  Future<List<QuizRankModel>> getTopN({int n = 3}) {
    return _$getTopNAsyncAction.run(() => super.getTopN(n: n));
  }

  final _$getTopNOffLineAsyncAction =
      AsyncAction('_QuizControllerBase.getTopNOffLine');

  @override
  Future<List<QuizRankModel>> getTopNOffLine() {
    return _$getTopNOffLineAsyncAction.run(() => super.getTopNOffLine());
  }

  final _$getPerguntasPorTopicosAsyncAction =
      AsyncAction('_QuizControllerBase.getPerguntasPorTopicos');

  @override
  Future<List<BasePerguntas>> getPerguntasPorTopicos(BaseTopicos topico) {
    return _$getPerguntasPorTopicosAsyncAction
        .run(() => super.getPerguntasPorTopicos(topico));
  }

  final _$carregarTopicosAsyncAction =
      AsyncAction('_QuizControllerBase.carregarTopicos');

  @override
  Future carregarTopicos() {
    return _$carregarTopicosAsyncAction.run(() => super.carregarTopicos());
  }

  final _$carregarPerguntasAsyncAction =
      AsyncAction('_QuizControllerBase.carregarPerguntas');

  @override
  Future carregarPerguntas() {
    return _$carregarPerguntasAsyncAction.run(() => super.carregarPerguntas());
  }

  final _$_carregarPerguntasAsyncAction =
      AsyncAction('_QuizControllerBase._carregarPerguntas');

  @override
  Future<String> _carregarPerguntas(BaseTopicos topico, String html) {
    return _$_carregarPerguntasAsyncAction
        .run(() => super._carregarPerguntas(topico, html));
  }

  final _$criarQuizAsyncAction = AsyncAction('_QuizControllerBase.criarQuiz');

  @override
  Future criarQuiz(BaseTopicos topico) {
    return _$criarQuizAsyncAction.run(() => super.criarQuiz(topico));
  }

  final _$_QuizControllerBaseActionController =
      ActionController(name: '_QuizControllerBase');

  @override
  dynamic incPerguntaAtual() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.incPerguntaAtual');
    try {
      return super.incPerguntaAtual();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic resetPerguntaAtual() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.resetPerguntaAtual');
    try {
      return super.resetPerguntaAtual();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic marcarAcerto() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.marcarAcerto');
    try {
      return super.marcarAcerto();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic initQuiz({int tempo}) {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.initQuiz');
    try {
      return super.initQuiz(tempo: tempo);
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic decTempoRestante() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.decTempoRestante');
    try {
      return super.decTempoRestante();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void startCountdown() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.startCountdown');
    try {
      return super.startCountdown();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void stopCountdown() {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.stopCountdown');
    try {
      return super.stopCountdown();
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  BaseTopicos getTopicoById(int index) {
    final _$actionInfo = _$_QuizControllerBaseActionController.startAction(
        name: '_QuizControllerBase.getTopicoById');
    try {
      return super.getTopicoById(index);
    } finally {
      _$_QuizControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
perguntas: ${perguntas},
topicoAtual: ${topicoAtual},
listaTopicos: ${listaTopicos},
basePerguntas: ${basePerguntas},
listaPerguntas: ${listaPerguntas},
perguntaAtual: ${perguntaAtual},
desafio: ${desafio},
qtdAcertos: ${qtdAcertos},
listaTop: ${listaTop},
tempoRestante: ${tempoRestante},
stopCount: ${stopCount}
    ''';
  }
}
