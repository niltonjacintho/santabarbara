import 'package:mobx/mobx.dart';

part 'quiz_game_controller.g.dart';

class QuizGameController = _QuizGameControllerBase with _$QuizGameController;

abstract class _QuizGameControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
