import 'package:flat_dialog/flat_dialog.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_beautiful_popup/main.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'quiz_game_controller.dart';
import '../quiz_controller.dart';

class QuizGamePage extends StatefulWidget {
  final String title;
  const QuizGamePage({Key key, this.title = "QuizGame"}) : super(key: key);

  @override
  _QuizGamePageState createState() => _QuizGamePageState();
}

class _QuizGamePageState
    extends ModularState<QuizGamePage, QuizGameController> {
  Color corBotaoPadrao = Colors.black45;
  Color corBotaoCerto = Colors.green[700];
  Color corBotaoErrado = Colors.red[700];
  QuizController quizController = Modular.get();

  @override
  Widget build(BuildContext context) {
    quizController = Modular.get();
    quizController.initQuiz();
    quizController.startCountdown();
    return Scaffold(
      backgroundColor: UtilsController().corFundoPadrao,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Text(
              quizController.topicoAtual.nome,
              style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w500,
                  color: Colors.yellow[400]),
            ),
            Center(
              child: Observer(
                builder: (_) => temporizador(),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Observer(
                  builder: (_) => Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      quizController.perguntaAtual.toString() +
                          ') ' +
                          quizController
                              .listaPerguntas[quizController.perguntaAtual]
                              .pergunta,
                      style: UtilsController().stylePerguntas,
                    ),
                  ),
                ),
              ),
            ),
            FutureBuilder(
                future: quizController
                    .getPerguntasPorTopicos(quizController.topicoAtual),
                initialData: [],
                builder: (context, snapshot) {
                  return listarPerguntas(context, snapshot);
                }),
          ],
        ),
      ),
    );
  }

  Widget temporizador() {
    return Text(
      quizController.getFormatTempoRestante(),
      style: UtilsController().stylePerguntas,
    );
  }

  Widget listarPerguntas(BuildContext context, AsyncSnapshot snapshot) {
    return Expanded(
      child: Observer(
        builder: (_) => ListView.builder(
          itemCount: quizController.listaPerguntas[quizController.perguntaAtual]
              .perguntasRespostas.length,
          itemBuilder: (context, i) {
            return Padding(
              padding: EdgeInsets.only(left: 5, right: 5, bottom: 10),
              child: ListTile(
                title: Padding(
                  padding: EdgeInsets.only(left: 1, right: 1),
                  child: RaisedButton(
                    color: Colors.blue[100],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.red)),
                    onPressed: () {
                      bool acertou = quizController
                          .listaPerguntas[quizController.perguntaAtual]
                          .perguntasRespostas[i]
                          .respostacerta;
                      if (acertou) {
                        quizController.marcarAcerto();
                      }
                      FlatDialog(
                        context: context,
                        type: acertou ? DialogType.success : DialogType.error,
                        title: acertou ? "PARABÉNS" : "ERROR",
                        desc: acertou
                            ? 'Gostei, você acertou. '
                            : 'Temos de estudar mais. Você terá mais sorte na próxima!',
                        buttons: [
                          FlatDialogButton(
                            child: const Text(
                              "Amazing!",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () => Navigator.pop(context),
                            width: 150,
                          )
                        ],
                      ).show();

                      // final popup = BeautifulPopup(
                      //   context: context,
                      //   template:
                      //       acertou ? TemplateGreenRocket : TemplateRedPacket,
                      // );
                      // popup.show(
                      //   close: Container(),
                      //   title: acertou ? 'PARABÉNS' : 'que pena',
                      //   content: Text(
                      //       acertou
                      //           ? 'Gostei, você acertou. '
                      //           : 'Temos de estudar mais. Você terá mais sorte na próxima!',
                      //       style: acertou
                      //           ? UtilsController().styleRespostaCerta
                      //           : UtilsController().styleRespostaErrada),
                      //   actions: [
                      //     popup.button(
                      //       label: 'Vamos prá próxima',
                      //       labelStyle: acertou
                      //           ? UtilsController().styleRespostaCerta
                      //           : UtilsController().styleRespostaErrada,
                      //       onPressed: () async {
                      //         //  setState(() {
                      //         if (quizController.perguntaAtual >=
                      //             quizController.qtdPerguntas) {
                      //           quizController.perguntaAtual = 1;
                      //           quizController.stopCountdown();
                      //           await quizController.salvarResultado();
                      //           //  await quizController.getTopN();
                      //           Navigator.of(context).pop();
                      //           Modular.to.pushNamed('/quizpagefinal');
                      //         } else {
                      //           quizController.incPerguntaAtual();
                      //           Navigator.of(context).pop();
                      //         }
                      //         //  });
                      //       },
                      //     ),
                      //   ],
                      // );
                    },
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                              quizController
                                  .listaPerguntas[quizController.perguntaAtual]
                                  .perguntasRespostas[i]
                                  .respostatexto,
                              style: UtilsController().styleRespostas)),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
