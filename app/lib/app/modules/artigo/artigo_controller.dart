import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/modules/artigo/repositories/artigo_repository.dart';

part 'artigo_controller.g.dart';

class ArtigoController = _ArtigoControllerBase with _$ArtigoController;
final ArtigoRepository repoArtigo = new ArtigoRepository();

abstract class _ArtigoControllerBase with Store {
  @observable
  int value = 0;

  @action
  Stream<QuerySnapshot> getListArtigos() {
    return repoArtigo.getListArtigos();
  }
  
}
