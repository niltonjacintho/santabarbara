import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/pages/artigoView/artigo_view_page.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:santabarbara/app/shared/grupos_service.dart';
import 'package:santabarbara/app/store/base_store.dart';
import 'package:flutter_cmoon_icons/flutter_cmoon_icons.dart';

class ArtigosPage extends StatefulWidget {
  final String title;
  const ArtigosPage({Key key, this.title = "Artigos"}) : super(key: key);

  @override
  _ArtigosPageState createState() => _ArtigosPageState();
}

class _ArtigosPageState extends ModularState<ArtigosPage, DocumentsController>
    with SingleTickerProviderStateMixin {
  Animation<double> _animation;
  AnimationController _animationController;
  int currentIndex;
  UtilsController util = Modular.get();
  GruposService gruposService = Modular.get();
 // BaseStore store = Modular.get();

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    gruposService.getGroupList(null);
    util.grupoAtivo = util.grupoAvisos;
    currentIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DocumentsController documentsController = Modular.get();

    Stream<QuerySnapshot> myStream =
        documentsController.getStreamByGroup(util.grupoAtivo.id);
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: menu(context),
      body: Builder(
        builder: (BuildContext context) => Container(
          color: util.corFundoAvisos,
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                title: Text(''),
                backgroundColor: Colors.red,
                expandedHeight: 200,
                floating: true,
                pinned: true,
                snap: true,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image.asset(
                    'assets/images/AvisosLogo.jpg',
                    fit: BoxFit.fitHeight,
                  ),
                  title: Text(util.grupoAtivo.titulo),
                ),
              ),
              StreamBuilder<QuerySnapshot>(
                stream: myStream,
                builder: (context, snapshot) {
                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Padding(
                          padding: EdgeInsets.all(10),
                          child: Card(
                            elevation: 10,
                            child: Column(
                              children: <Widget>[
                                Image.network(
                                  snapshot.data.docs[index].get('imagem'),

                                  //  height: 80,
                                  fit: BoxFit.contain,
                                  alignment: Alignment
                                      .center, // .centerLeft, // .topCenter,
                                ),
                                ListTile(
                                  onTap: () {
                                    documentsController.documents =
                                        convertToDoc(
                                            snapshot.data.docs[index].data());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ArtigoViewPage(
                                                    title: util
                                                        .grupoAtivo.titulo)));
                                  },
                                  title: Text(
                                    snapshot.data.docs[index].get('titulo'),
                                    style: TextStyle(fontSize: 24),
                                  ),
                                  subtitle: Text(
                                      snapshot.data.docs[index]
                                          .get('subtitulo'),
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ],
                            ),
                            //),
                          ),
                        );
                      },
                      childCount:
                          snapshot.hasData ? snapshot.data.docs.length : 0,
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  DocumentsModel convertToDoc(Map<String, dynamic> value) {
    DocumentsModel doc = new DocumentsModel();
    doc.titulo = value['titulo'];
    doc.subtitulo = value['subtitulo'];
    doc.conteudo = value['conteudo'];
    doc.imagem = value['imagem'];
    doc.ativo = true;
    if (value['id'] != null) {
      doc.id = value["id"];
    } else {
      doc.id = new DateTime.now().millisecondsSinceEpoch.toString();
    }
    return doc;
  }

  Widget menu(BuildContext context) {
    return FloatingActionBubble(
      // Menu items
      items: <Bubble>[
        // Floating action menu item
        Bubble(
          title: "Avisos Paroquiais",
          iconColor: Colors.white,
          bubbleColor: Colors.blue[900],
          icon: IconMoon.icon_document,
          titleStyle: estilo,
          onPress: () {
            _animationController.reverse();
            // util.grupoAtivo = util?.grupoAvisos ?? null;
            setState(() {
              util.grupoAtivo = gruposService.getGroupById("avisos");
            });

            print(1);
          },
        ),
        // Floating action menu item
        Bubble(
          title: "Notícias Católicas",
          iconColor: Colors.white,
          bubbleColor: Colors.yellow[500],
          icon: Icons.people,
          titleStyle: TextStyle(fontSize: 25, color: Colors.blue[900]),
          onPress: () {
            //   _index = 1;
            _animationController.reverse();
            setState(() {
              util.grupoAtivo = gruposService.getGroupById("noticias");
            });
          },
        ),
        //Floating action menu item
        Bubble(
          title: "Nossos Párocos",
          iconColor: Colors.white,
          bubbleColor: Colors.red[900],
          icon: Icons.home,
          titleStyle: estilo,
          onPress: () {
            _animationController.reverse();
            setState(() {
              util.grupoAtivo = gruposService.getGroupById("padres");
            });
          },
        ),
        Bubble(
          title: "Nossos Igreja",
          iconColor: Colors.white,
          bubbleColor: Colors.green[900],
          icon: Icons.home,
          titleStyle: estilo,
          onPress: () {
            _animationController.reverse();
            setState(() {
              util.grupoAtivo = gruposService.getGroupById("eventos");
            });
          },
        ),
        Bubble(
          title: "Horários",
          iconColor: Colors.white,
          bubbleColor: Colors.deepPurple[800],
          icon: Icons.home,
          titleStyle: estilo,
          onPress: () {
            _animationController.reverse();
            setState(() {
              util.grupoAtivo = gruposService.getGroupById("horarios");
            });
          },
        )
      ],

      // animation controller
      animation: _animation,

      // On pressed change animation state
      onPress: _animationController.isCompleted
          ? _animationController.reverse
          : _animationController.forward,

      // Floating Action button Icon color
      iconColor: Colors.blue,

      // Flaoting Action button Icon
     // icon: AnimatedIcons.add_event,
    );
  }

  Widget box(BuildContext context) {
    return RaisedButton(
      child: Text("Alert - Tap to dismiss"),
      color: Colors.yellow,
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  TextStyle estilo = TextStyle(fontSize: 25, color: Colors.white);
}
