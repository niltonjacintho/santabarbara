abstract class IArtigoRepository {
  getListArtigos();
  gotoArtigo();
  getListArtigoByFilter(String filtro);
  getListArtigoByCategoria(String filtro);
}
