import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:santabarbara/app/modules/artigo/repositories/iartigo_repository_interface.dart';

class ArtigoRepository implements IArtigoRepository {
  @override
  getListArtigoByCategoria(String filtro) {
    throw UnimplementedError();
  }

  @override
  Stream<QuerySnapshot> getListArtigos() {
    return _getStoreData();
  }

  @override
  gotoArtigo() {
    throw UnimplementedError();
  }

  Stream<QuerySnapshot> _getStoreData() {
    return FirebaseFirestore.instance
        .collection('artigos')
        .where('grupo', isEqualTo: '200')
        .where('ativo', isEqualTo: true)
        .snapshots();
  }

  @override
  getListArtigoByFilter(String filtro) {
    throw UnimplementedError();
  }
}
