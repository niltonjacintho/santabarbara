import 'package:flutter/material.dart';
import 'package:santabarbara/app/models/artigo_model.dart';

class ArtigoTemplate extends StatelessWidget {
  final Artigos artigo;

  ArtigoTemplate({this.artigo});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.greenAccent,
            ),
            title: Text(
              artigo.titulo,
              style: TextStyle(fontSize: 22.00),
            ),
            subtitle: Text(artigo.subtitulo),
            //trailing: Text(movie.year),
          )
        ],
      ),
    );
  }
}
