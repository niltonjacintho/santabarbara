import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class ArtigoTemplateView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UtilsController util = Modular.get();
    DocumentsController documentsController = Modular.get();
    // backgr
    // backgroundcolor:

    Size size = MediaQuery.of(context).size;
    return SimpleDialog(
      backgroundColor: util.corFundoArtigo,
      children: <Widget>[
        Container(
            width: size.width,
            height: size.height, // * 0.9,
            margin: EdgeInsets.only(top: 0, left: 2, right: 2),
            child: ListView(
              children: [
                Card(
                  color: util.corFundoArtigo,
                  semanticContainer: true,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  // elevation: 45,
                  //   margin: EdgeInsets.only(top: 1, left: 2, right: 2, bottom: 2),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        child: Image.network(
                          documentsController.documents.imagem,
                          fit: BoxFit.fitHeight,
                          alignment: Alignment.topCenter,
                        ),
                        height: 300,
                        width: 400,
                      ),
                      Text(
                        documentsController.documents.titulo,
                        style: TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                      Text(
                        documentsController.documents.subtitulo,
                        style: TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.normal,
                          fontSize: 30,
                        ),
                      ),
                      Divider(
                        height: 10,
                        color: Colors.grey,
                      ),
                      SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            documentsController.documents.conteudo,
                            maxLines: 12,
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontWeight: FontWeight.normal,
                              fontSize: 30,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ],
    );
  }
}
