import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/modules/artigo/artigo_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/artigo/artigo_page.dart';

class ArtigoModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => ArtigoController()),
        Bind((i) => DocumentsController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => ArtigosPage()),
      ];

  static Inject get to => Inject<ArtigoModule>(); //>.of(;
}
