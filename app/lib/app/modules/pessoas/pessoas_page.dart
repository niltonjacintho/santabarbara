import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'pessoas_controller.dart';

class PessoasPage extends StatefulWidget {
  final String title;
  const PessoasPage({Key key, this.title = "Pessoas"}) : super(key: key);

  @override
  _PessoasPageState createState() => _PessoasPageState();
}

class _PessoasPageState extends ModularState<PessoasPage, PessoasController> {
  //use 'controller' variable to access controller
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  UtilsController _util = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                FormBuilder(
                  key: _fbKey,
                  initialValue: {
                    'date': DateTime.now(),
                    'accept_terms': false,
                  },
                //  autovalidateMode: AutovalidateMode.always,
                  child: Expanded(
                    child: Column(
                      children: <Widget>[
                        _util.campoTextoPadrao(label: 'Nome', atributo: 'nome'),
                        SizedBox(height: _util.espacoPadrao),
                        _util.campoTextoPadrao(
                            label: 'Apelido', atributo: 'apelido'),
                        SizedBox(height: _util.espacoPadrao),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    0, 10, 10, 0),
                                child: _util.campoTextoPadrao(
                                    label: 'E. Civil', atributo: 'nome'),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    00, 10, 10, 0),
                                child: _util.campoDataPadrao(
                                    label: 'Nascimento', atributo: 'nome'),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: _util.espacoPadrao),
                        _util.campoTextoPadrao(
                            label: 'Endereco', atributo: 'logradouro'),
                        SizedBox(height: _util.espacoPadrao),
                        _util.campoTextoPadrao(
                            label: 'Bairro', atributo: 'bairro'),
                      ],
                    ),
                  ),
                ),
                Row(
                  children: <Widget>[
                    MaterialButton(
                      child: Text("Submit"),
                      onPressed: () {
                        if (_fbKey.currentState.saveAndValidate()) {
                          print(_fbKey.currentState.value);
                        }
                      },
                    ),
                    MaterialButton(
                      child: Text("Reset"),
                      onPressed: () {
                        _fbKey.currentState.reset();
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
