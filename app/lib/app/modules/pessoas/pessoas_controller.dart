import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/pessoas_model.dart';

part 'pessoas_controller.g.dart';

class PessoasController = _PessoasControllerBase with _$PessoasController;

abstract class _PessoasControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  @observable
  DadosPessoais pessoa = new DadosPessoais();

  @action
  salvarPessoa(DadosPessoais pessoa) {}

  @action
  removerPessoa(DadosPessoais pessoa) {}
}
