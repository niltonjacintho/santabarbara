// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pessoas_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PessoasController on _PessoasControllerBase, Store {
  final _$valueAtom = Atom(name: '_PessoasControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$pessoaAtom = Atom(name: '_PessoasControllerBase.pessoa');

  @override
  DadosPessoais get pessoa {
    _$pessoaAtom.reportRead();
    return super.pessoa;
  }

  @override
  set pessoa(DadosPessoais value) {
    _$pessoaAtom.reportWrite(value, super.pessoa, () {
      super.pessoa = value;
    });
  }

  final _$_PessoasControllerBaseActionController =
      ActionController(name: '_PessoasControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_PessoasControllerBaseActionController.startAction(
        name: '_PessoasControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_PessoasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic salvarPessoa(DadosPessoais pessoa) {
    final _$actionInfo = _$_PessoasControllerBaseActionController.startAction(
        name: '_PessoasControllerBase.salvarPessoa');
    try {
      return super.salvarPessoa(pessoa);
    } finally {
      _$_PessoasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic removerPessoa(DadosPessoais pessoa) {
    final _$actionInfo = _$_PessoasControllerBaseActionController.startAction(
        name: '_PessoasControllerBase.removerPessoa');
    try {
      return super.removerPessoa(pessoa);
    } finally {
      _$_PessoasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
pessoa: ${pessoa}
    ''';
  }
}
