import 'pessoas_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'pessoas_page.dart';

class PessoasModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PessoasController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => PessoasPage()),
      ];

  static Inject get to => Inject<PessoasModule>(); //>.of(;
}
