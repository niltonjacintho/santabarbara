import 'remedios_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'remedios_page.dart';

class RemediosModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => RemediosController()),
        // $RemediosController,
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => RemediosPage()),
      ];

  static Inject get to => Inject<RemediosModule>(); //>.of(;
}
