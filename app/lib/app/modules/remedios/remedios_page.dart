import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'remedios_controller.dart';

class RemediosPage extends StatefulWidget {
  final String title;
  const RemediosPage({Key key, this.title = "Remedios"}) : super(key: key);

  @override
  _RemediosPageState createState() => _RemediosPageState();
}

class _RemediosPageState
    extends ModularState<RemediosPage, RemediosController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
