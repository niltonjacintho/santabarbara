import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/remedios.model.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

part 'remedios_controller.g.dart';

@Injectable()
class RemediosController = _RemediosControllerBase with _$RemediosController;

abstract class _RemediosControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  var listaItens =
      '0-9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z'.split(",");

  String urlBase = 'https://consultaremedios.com.br/bulas/';
  List<int> paginas = [];
  List<Remedios> listaRemedios = [];

  @action
  addRemedios(Remedios remedio) {
    var idx =
        listaRemedios.indexWhere((element) => element.nome == remedio.nome);
    if (idx >= 0) {
      listaRemedios[idx].fabricantes.add(remedio.fabricantes[0]);
    } else {
      listaRemedios.add(remedio);
    }
  }

  montarListaRemedios() async {
    print('Montando Array');
    paginas = new List<int>();
    final campoProcura = 'product-block__title';
    final campoProcuraEmpresa = 'product-block__factory';
    final campoSpan = '</span>';
    Response response;
    String conteudo;

    for (var i = 0; i <= listaItens.length - 1; i++) {
      var contaPagina = 1;
      bool existeRemedios = true;
      response =
          await Dio().get(urlBase + "/${listaItens[i]}?pagina=$contaPagina");
      conteudo = response.data;
      existeRemedios = (conteudo.indexOf('product-block__title') != -1);
      while (existeRemedios) {
        while (conteudo.indexOf('product-block__title') != -1) {
          try {
            Remedios remedio = new Remedios();
            remedio.fabricantes = new List<Fabricantes>();
            conteudo = conteudo.substring(conteudo.indexOf(campoProcura));
            conteudo = conteudo.substring(conteudo.indexOf(campoSpan));
            remedio.nome = conteudo.substring(7, conteudo.indexOf('</a>'));
            remedio.id = md5.convert(utf8.encode(remedio.nome)).toString();
            Fabricantes fabricantes = new Fabricantes();
            if (conteudo.indexOf(campoProcuraEmpresa) <=
                conteudo.indexOf(campoProcura)) {
              conteudo =
                  conteudo.substring(conteudo.indexOf(campoProcuraEmpresa));
              conteudo = conteudo.substring(conteudo.indexOf(campoSpan));
              // fabricantes.nome =
              //     conteudo.substring(7, conteudo.indexOf('</div>'));
              fabricantes.nome =
                  conteudo.substring(7, conteudo.indexOf('</div>'));
            } else {
              Fabricantes fabricantes = new Fabricantes();
              fabricantes.nome = "";
            }
            remedio.fabricantes.add(fabricantes);
            addRemedios(remedio);
          } catch (e) {
            print(e);
          }
        }
        contaPagina++;
        response =
            await Dio().get(urlBase + "/${listaItens[i]}?pagina=$contaPagina");
        conteudo = response.data;
        existeRemedios = (conteudo.indexOf('product-block__title') != -1);
      }
    }
    await gravarRemedios();
  }

  // XgravarRemedios() async {
  //   CollectionReference db = FirebaseFirestore.instance.collection("remedios/");
  //   db.add({"id": "00001", "name": "Tokyo", "country": "Japan"});
  // }

  gravarRemedios() async {
    // criei mas não testei!
    CollectionReference db = FirebaseFirestore.instance.collection("remedios/");
    Map<String, dynamic> map =
        Map.fromIterable(listaRemedios, key: (v) => v[0], value: (v) => v[1]);
    // listaRemedios.map((v) => v.toJson());
    db.add(map);
    for (var item in listaRemedios) {
      String cod = md5.convert(utf8.encode(item.nome)).toString();
      DocumentReference reference =
          FirebaseFirestore.instance.doc("remedios/$cod");

      try {
        await reference
            .set(item.toJson())
            .then((result) => {})
            .catchError((err) => {
                  print('========================================'),
                  print('ERROR $err'),
                  print(err),
                });
      } catch (e) {
        print(e);
      }
    }
    listaRemedios = [];
  }
}
