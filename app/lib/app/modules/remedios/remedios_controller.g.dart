// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remedios_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RemediosController on _RemediosControllerBase, Store {
  final _$valueAtom = Atom(name: '_RemediosControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$_RemediosControllerBaseActionController =
      ActionController(name: '_RemediosControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_RemediosControllerBaseActionController.startAction(
        name: '_RemediosControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_RemediosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addRemedios(Remedios remedio) {
    final _$actionInfo = _$_RemediosControllerBaseActionController.startAction(
        name: '_RemediosControllerBase.addRemedios');
    try {
      return super.addRemedios(remedio);
    } finally {
      _$_RemediosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value}
    ''';
  }
}
