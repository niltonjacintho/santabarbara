// import 'noticias_catolicas_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'noticias_catolicas_page.dart';

class NoticiasCatolicasModule extends Module {
  @override
  List<Bind> get binds => [
        // $NoticiasCatolicasController,
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => NoticiasCatolicasPage()),
      ];

  static Inject get to => Inject<NoticiasCatolicasModule>(); //>.of(;
}
