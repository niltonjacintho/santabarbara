import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'noticias_catolicas_controller.dart';

class NoticiasCatolicasPage extends StatefulWidget {
  final String title;
  const NoticiasCatolicasPage({Key key, this.title = "NoticiasCatolicas"})
      : super(key: key);

  @override
  _NoticiasCatolicasPageState createState() => _NoticiasCatolicasPageState();
}

class _NoticiasCatolicasPageState
    extends ModularState<NoticiasCatolicasPage, NoticiasCatolicasController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
