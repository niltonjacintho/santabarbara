import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'noticias_catolicas_controller.g.dart';

@Injectable()
class NoticiasCatolicasController = _NoticiasCatolicasControllerBase
    with _$NoticiasCatolicasController;

abstract class _NoticiasCatolicasControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
