// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pastorais_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PastoraisController on _PastoraisControllerBase, Store {
  final _$valueAtom = Atom(name: '_PastoraisControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$listaPastoraisAtom =
      Atom(name: '_PastoraisControllerBase.listaPastorais');

  @override
  List<dynamic> get listaPastorais {
    _$listaPastoraisAtom.reportRead();
    return super.listaPastorais;
  }

  @override
  set listaPastorais(List<dynamic> value) {
    _$listaPastoraisAtom.reportWrite(value, super.listaPastorais, () {
      super.listaPastorais = value;
    });
  }

  final _$_PastoraisControllerBaseActionController =
      ActionController(name: '_PastoraisControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_PastoraisControllerBaseActionController.startAction(
        name: '_PastoraisControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_PastoraisControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
listaPastorais: ${listaPastorais}
    ''';
  }
}
