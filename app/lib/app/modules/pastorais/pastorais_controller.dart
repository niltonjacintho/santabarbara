import 'dart:convert';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';
import 'package:mobx/mobx.dart';

part 'pastorais_controller.g.dart';

class PastoraisController = _PastoraisControllerBase with _$PastoraisController;

abstract class _PastoraisControllerBase with Store {
  @observable
  int value = 0;

  @observable
  List listaPastorais;

  @action
  void increment() {
    value++;
  }


  uploadEventos(BuildContext context) async {
    String _data = await DefaultAssetBundle.of(context)
        .loadString("assets/data/pastorais.json");
    var data = json.decode(_data);
    // print(data);
    for (var item in data["pastorais"]) {
      print(item);
      DocumentReference reference =
          FirebaseFirestore.instance.doc("pastorais/${item["codigo"]}");

      await reference
          .set(toJson(item))
          .then((result) => {})
          .catchError((err) => print('ERROR $err'));
    }
  }

  Map<String, dynamic> toJson(p) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['titulo'] = p["titulo"];
    data['sigla'] = p["sigla"];
    data['ordem'] = p["ordem"];
    data['codigo'] = p["codigo"];
    data['conteudo'] = lorem(
        paragraphs: new Random().nextInt(8), words: Random().nextInt(150));
    return data;
  }
}
