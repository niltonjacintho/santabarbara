import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';
// import 'package:santabarbara/app/models/artigo_model.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/shared/grupos_service.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/documentsModel.dart';
import 'package:santabarbara/app/pages/artigoView/artigo_view_page.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Carousel Slider',
      home: PastoraisPage(title: 'Flutter Carousel Slider'),
    );
  }
}

class PastoraisPage extends StatefulWidget {
  final String title = '';

  PastoraisPage({Key key, title}) : super(key: key);

  @override
  _PastoraisPageState createState() => _PastoraisPageState();
}

class _PastoraisPageState extends State<PastoraisPage> {
  UtilsController util = Modular.get();
  final String grupo = 'pastorais';
  final List<Color> colors = [
    Colors.red[900],
    Colors.orange[500],
    Colors.red[300],
    Colors.green[900],
    Colors.blue[900],
    Colors.indigo[900],
    Colors.purple[800],
  ];

  final  _sliderKey = GlobalKey();
  GruposService grupoService = Modular.get();
  DocumentsController documentsController = Modular.get();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('artigos')
            .where('grupo', isEqualTo: grupo)
            .snapshots(),
        builder: (context, snap) {
          if (!snap.hasData) {
            return const Text('carregando...');
          }
          if (snap.data.docs.length <= 0) 
          {
            return const Text('Sem dados...');
          }
          return CarouselSlider.builder(
            key: _sliderKey,
            unlimitedMode: true,
            slideBuilder: (index) {
              return Container(
                alignment: Alignment.topCenter,
                color: colors[index],
                child: Stack(
                  children: [
                    GestureDetector(
                        child: Image.network(
                          snap.data.docs[index]["imagem"],
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                    : null,
                              ),
                            );
                          },
                        ),
                        onTap: () => {
                              documentsController.documents =
                                  convertToDoc(snap.data.docs[index]),
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => new ArtigoViewPage(
                                          title: snap.data.docs[index]
                                              ["titulo"],
                                        )),
                              ),
                            }),
                    Center(
                      child: Text(
                        snap.data.docs[index]["titulo"],
                        style: TextStyle(fontSize: 40, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              );
            },
            slideTransform: CubeTransform(),
            slideIndicator: CircularSlideIndicator(
              padding: EdgeInsets.only(bottom: 32),
            ),
            itemCount: colors.length,
          );
        },
      ),
    );
  }

  DocumentsModel convertToDoc(value) {
    DocumentsModel doc = new DocumentsModel();
    doc.titulo = value["titulo"];
    doc.subtitulo = value["subtitulo"];
    doc.conteudo = value["conteudo"];
    doc.imagem = value["imagem"];
    doc.ativo = true;
    if (value["id"] != null) {
      doc.id = value["id"];
    } else {
      doc.id = new DateTime.now().millisecondsSinceEpoch.toString();
    }
    return doc;
  }
}
