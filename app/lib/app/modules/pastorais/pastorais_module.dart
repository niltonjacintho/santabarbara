import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/modules/pastorais/pastorais_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/pastorais/pastorais_page.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class PastoraisModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PastoraisController()),
        Bind((i) => UtilsController()),
        Bind((i) => DocumentsController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => PastoraisPage()),
      ];

  static Inject get to => Inject<PastoraisModule>(); //>.of(;
}
