import 'velaacesa/velaacesa_controller.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_page.dart';

class VelavirtualModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => VelaacesaController()),
        Bind((i) => VelavirtualController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => VelavirtualPage()),
      ];

  static Inject get to => Inject<VelavirtualModule>(); //>.of(;
}
