import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'velavirtual_controller.dart';

class VelavirtualPage extends StatefulWidget {
  final String title;
  const VelavirtualPage({Key key, this.title = "Velavirtual"})
      : super(key: key);

  @override
  _VelavirtualPageState createState() => _VelavirtualPageState();
}

class _VelavirtualPageState
    extends ModularState<VelavirtualPage, VelavirtualController> {
  //use 'controller' variable to access controller
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  //final format = DateFormat("dd/MM/yyyy");
  VelavirtualController velaController = Modular.get();
  @override
  Widget build(BuildContext context) {
    var _selectedIndex = 0;
    velaController..getTiposPedidos();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: (BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.send, size: 30),
            label: 'Acender a Vela',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.cancel),
            label:'Não acender',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (value) async {
          if (value == 0) {
            if (_fbKey.currentState.saveAndValidate()) {
              print(_fbKey.currentState.value);
              await velaController.gravar();
              Modular.to.pushNamed('/velaacendendo');
              // Alert(
              //   context: context,
              //   type: AlertType.success,
              //   title: "Velário",
              //   desc: "Sua vela foi acesa!",
              //   buttons: [
              //     DialogButton(
              //       child: Text(
              //         "Ver a vela",
              //         style: TextStyle(color: Colors.white, fontSize: 20),
              //       ),
              //       onPressed: () => Modular.to.pushNamed('/vela'),
              //       width: 120,
              //     )
              //   ],
              // ).show();
              setState(() {
                var a = 1;
                print(a);
              });
              Modular.to.pushNamed('/home');
            }
          } else {
            Modular.to.pushNamed('/home');
          }
          print(value);
        },
      )),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            FormBuilder(
              // context,
              key: _fbKey,
             // readOnly: false,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 15),
                  FormBuilderTextField(
                    name: "para",
                    decoration: InputDecoration(
                      labelText: "Você esta pedindo para quem?",
                    ),
                    onChanged: (value) {
                      velaController.velaAtual.destinatario = value;
                    },
                    valueTransformer: (text) {
                      return text == null ? null : num.tryParse(text);
                    },
                    // validators: [
                    //   FormBuilderValidators.required(
                    //       errorText: 'Poxa, nós precisamos desta informação!'),
                    //   FormBuilderValidators.minLength(2, allowEmpty: false),
                    // ],
                    keyboardType: TextInputType.number,
                  ),
                  FormBuilderTextField(
                    name: "texto",
                    maxLines: 5,
                    decoration: InputDecoration(
                      labelText: "Escreva o seu pedido.",
                    ),
                    onChanged: (value) {
                      velaController.velaAtual.texto = value;
                    },
                    valueTransformer: (text) {
                      return text == null ? null : num.tryParse(text);
                    },
                    // validators: [
                    //   FormBuilderValidators.required(
                    //       errorText: 'Poxa, nós precisamos desta informação!'),
                    //   FormBuilderValidators.minLength(5,
                    //       allowEmpty: false,
                    //       errorText:
                    //           'Aqui você precisa pelo menos 5 caracteres!'),
                    // ],
                    keyboardType: TextInputType.text,
                  ),
                  // Observer(
                  //   builder: (_) => FormBuilderDropdown(
                  //     attribute: "tipo",
                  //     decoration: InputDecoration(
                  //       labelText: "Qual a intensão do pedido ?",
                  //       border: UnderlineInputBorder(
                  //         borderSide: BorderSide(
                  //           color: Colors.green,
                  //           width: 20,
                  //         ),
                  //       ),
                  //     ),
                  //     onChanged: (value) {
                  //       velaController.velaAtual.intensao = value;
                  //     },
                  //     validators: [
                  //       FormBuilderValidators.required(
                  //           errorText: 'Nós precisamos desta informação!')
                  //     ],
                  //     items: velaController.listaTipos
                  //         .map((tipo) => DropdownMenuItem(
                  //               value: tipo,
                  //               child: Text('$tipo'),
                  //             ))
                  //         .toList(),
                  //   ),
                  // ),
                  SizedBox(height: 15),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
