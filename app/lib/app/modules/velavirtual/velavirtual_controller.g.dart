// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'velavirtual_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$VelavirtualController on _VelavirtualControllerBase, Store {
  final _$velaAtualAtom = Atom(name: '_VelavirtualControllerBase.velaAtual');

  @override
  VelaModel get velaAtual {
    _$velaAtualAtom.reportRead();
    return super.velaAtual;
  }

  @override
  set velaAtual(VelaModel value) {
    _$velaAtualAtom.reportWrite(value, super.velaAtual, () {
      super.velaAtual = value;
    });
  }

  final _$valueAtom = Atom(name: '_VelavirtualControllerBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$qtdVelasAcesasAtom =
      Atom(name: '_VelavirtualControllerBase.qtdVelasAcesas');

  @override
  int get qtdVelasAcesas {
    _$qtdVelasAcesasAtom.reportRead();
    return super.qtdVelasAcesas;
  }

  @override
  set qtdVelasAcesas(int value) {
    _$qtdVelasAcesasAtom.reportWrite(value, super.qtdVelasAcesas, () {
      super.qtdVelasAcesas = value;
    });
  }

  final _$listaTiposAtom = Atom(name: '_VelavirtualControllerBase.listaTipos');

  @override
  List<dynamic> get listaTipos {
    _$listaTiposAtom.reportRead();
    return super.listaTipos;
  }

  @override
  set listaTipos(List<dynamic> value) {
    _$listaTiposAtom.reportWrite(value, super.listaTipos, () {
      super.listaTipos = value;
    });
  }

  final _$velasAcesasAtom =
      Atom(name: '_VelavirtualControllerBase.velasAcesas');

  @override
  List<dynamic> get velasAcesas {
    _$velasAcesasAtom.reportRead();
    return super.velasAcesas;
  }

  @override
  set velasAcesas(List<dynamic> value) {
    _$velasAcesasAtom.reportWrite(value, super.velasAcesas, () {
      super.velasAcesas = value;
    });
  }

  final _$getTiposPedidosAsyncAction =
      AsyncAction('_VelavirtualControllerBase.getTiposPedidos');

  @override
  Future getTiposPedidos() {
    return _$getTiposPedidosAsyncAction.run(() => super.getTiposPedidos());
  }

  final _$gravarAsyncAction = AsyncAction('_VelavirtualControllerBase.gravar');

  @override
  Future gravar() {
    return _$gravarAsyncAction.run(() => super.gravar());
  }

  final _$getVelasAcesasAsyncAction =
      AsyncAction('_VelavirtualControllerBase.getVelasAcesas');

  @override
  Future<List<dynamic>> getVelasAcesas(String email) {
    return _$getVelasAcesasAsyncAction.run(() => super.getVelasAcesas(email));
  }

  final _$_VelavirtualControllerBaseActionController =
      ActionController(name: '_VelavirtualControllerBase');

  @override
  void increment() {
    final _$actionInfo = _$_VelavirtualControllerBaseActionController
        .startAction(name: '_VelavirtualControllerBase.increment');
    try {
      return super.increment();
    } finally {
      _$_VelavirtualControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
velaAtual: ${velaAtual},
value: ${value},
qtdVelasAcesas: ${qtdVelasAcesas},
listaTipos: ${listaTipos},
velasAcesas: ${velasAcesas}
    ''';
  }
}
