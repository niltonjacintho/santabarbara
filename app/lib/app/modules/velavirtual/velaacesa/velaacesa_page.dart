import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'velaacesa_controller.dart';
import 'package:video_player/video_player.dart';

class VelaacesaPage extends StatefulWidget {
  final String title;
  const VelaacesaPage({Key key, this.title = "Velaacesa"}) : super(key: key);

  @override
  _VelaacesaPageState createState() => _VelaacesaPageState();
}

class _VelaacesaPageState
    extends ModularState<VelaacesaPage, VelaacesaController> {
  //use 'controller' variable to access controller
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    // _controller = VideoPlayerController.network(
    //    "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4");
    _controller = VideoPlayerController.asset("videos/acendendo.mp4");
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(1.0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Center(
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: VideoPlayer(_controller),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
