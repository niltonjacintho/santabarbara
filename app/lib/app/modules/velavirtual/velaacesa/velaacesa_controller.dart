import 'package:mobx/mobx.dart';

part 'velaacesa_controller.g.dart';

class VelaacesaController = _VelaacesaControllerBase with _$VelaacesaController;

abstract class _VelaacesaControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
