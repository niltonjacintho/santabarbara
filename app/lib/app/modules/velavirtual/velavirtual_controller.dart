import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/vela_model.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'package:uuid/uuid.dart';

part 'velavirtual_controller.g.dart';

class VelavirtualController = _VelavirtualControllerBase
    with _$VelavirtualController;

abstract class _VelavirtualControllerBase with Store {
  @observable
  VelaModel velaAtual = new VelaModel();

  @observable
  int value = 0;

  @observable
  int qtdVelasAcesas = 0;

  @observable
  List listaTipos = [];

  @observable
  List velasAcesas = [];

  @action
  void increment() {
    value++;
  }

  var uuid = Uuid();
  AuthController authController = Modular.get();
  UtilsController utilsController = Modular.get();

  @action
  getTiposPedidos() async {
    QuerySnapshot qShot =
        await FirebaseFirestore.instance.collection('pedidos_tipos').get();
    listaTipos = qShot.docs.map((doc) => doc.get('tipo')).toList();
    return qShot.docs.map((doc) => doc.get('tipo')).toList();
  }

  @action
  gravar() async {
    if (velaAtual.id == null || velaAtual.id == '') {
      velaAtual.id = uuid.v4();
    }
    DocumentReference reference =
        FirebaseFirestore.instance.doc("vela_virtual/" + velaAtual.id);
    velaAtual.dataInclusao = velaAtual.dataInclusao == null
        ? DateTime.now()
        : velaAtual.dataInclusao;
    velaAtual.solicitanteemail = authController.user.providerData[0].email;
    velaAtual.solicitantenome =
        authController.user.displayName;
    velaAtual.dataAlteracao = DateTime.now();
    velaAtual.data = DateTime.now();
    velaAtual.minutosrestantes = utilsController.tempoVelaAcesa;
    reference
        .set(velaAtual.toJson())
        .then((result) => {getVelasAcesas(velaAtual.solicitanteemail)})
        .catchError((err) => print('ERROR $err'));
  }

  @action
  Future<List> getVelasAcesas(String email) async {
    velasAcesas = new List();
    qtdVelasAcesas = 0;
    await for (var snapshot in FirebaseFirestore.instance
        .collection('vela_virtual')
        .where('solicitanteemail', isEqualTo: email)
        .snapshots()) {
      for (var element in snapshot.docs) {
        print(element.data());
        Timestamp t = element['data'];
        VelaModel m = new VelaModel(
            id: element.get('id'),
            intensao: element.get('intensao'),
            destinatario: element.get('destinatario'),
            data: t.toDate());
        Duration difference = DateTime.now().difference(t.toDate());
        if (difference.inMinutes <= utilsController.tempoVelaAcesa) {
          velasAcesas.add(m);
          qtdVelasAcesas++;
        }
      }
    }
    return velasAcesas;
  }
}
