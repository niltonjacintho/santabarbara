import 'package:santabarbara/app/models/homeoptions_model.dart';

abstract class IHomeRepository {
  List<Homeoptions> getMenuOptions();
  gotoOption(Homeoptions home);
}
