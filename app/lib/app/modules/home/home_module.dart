import 'package:santabarbara/app/modules/agenda/agenda_controller.dart';
import 'package:santabarbara/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/home/home_page.dart';
import 'package:santabarbara/app/modules/quiz/quiz_controller.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_controller.dart';

class HomeModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
        Bind((i) => AgendaController()),
        Bind((i) => VelavirtualController()),
        Bind((i) => QuizController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>(); //>.of(;
}
