import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/opcoesMenu.model.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  @observable
  int value = 0;

  List<OpcoesMenu> listaMenu = [];

  //AnalyticsService _analytics = new AnalyticsService();

  @action
  void gotoDestino(String destino, BuildContext context) {
    try {
      //await _analytics.getAnalyticsObserver().navigator. . .setUserId(userId);
      Modular.to.pushNamed('/$destino');
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Text("Em desenvolvimento"),
              content: new Text(
                  "Estamos trabalhando para levar mais informações para você!"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Fechar"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  fillMenu() {
    listaMenu = new List<OpcoesMenu>();
    listaMenu.add(
      new OpcoesMenu(
          image: 'assets/images/avisos.png',
          titulo: 'Fique por dentro',
          ordem: 01,
          destino: 'artigo',
          grupo: 100,
          subtitulo:
              'Aqui você encontra todos os avisos e mensagens da paróquia. Tudo separado em grupos especialmente para você!',
          cardColor: Colors.blue),
    );

    listaMenu.add(new OpcoesMenu(
      image: 'assets/images/vela.png',
      titulo: 'Vela Virtual',
      ordem: 09,
      destino: 'velavirtual',
      grupo: 0,
      cardColor: Colors.red[900],
      textColor: Colors.white,
      subtitulo: ' ..',
    ));

    listaMenu.add(new OpcoesMenu(
        image: 'assets/images/paroquia.png',
        titulo: 'Nossas Paróquias',
        ordem: 12,
        destino: 'paroquias',
        grupo: 0,
        cardColor: Colors.green[900],
        textColor: Colors.white,
        subtitulo: ''));

    listaMenu.add(new OpcoesMenu(
      image: 'assets/images/quiz.png',
      titulo: 'Quiz',
      ordem: 3,
      destino: 'quiz',
      grupo: 0,
      subtitulo: ' ..',
    ));

    // listaMenu.add(new OpcoesMenu(
    //   image: 'assets/images/padres.png',
    //   titulo: 'Nossos párocos',
    //   ordem: 4,
    //   destino: 'padres',
    //   grupo: 0,
    //   subtitulo: ' ..',
    // ));

    listaMenu.add(new OpcoesMenu(
      image: 'assets/images/agenda.png',
      titulo: 'Agenda Paroquial',
      ordem: 5,
      destino: 'agenda',
      grupo: 0,
      subtitulo: ' ..',
    ));

    listaMenu.add(new OpcoesMenu(
      image: 'assets/images/pastorais.png',
      titulo: 'Nossas Pastorais',
      ordem: 5,
      destino: 'pastorais',
      grupo: 0,
      subtitulo: '.. ',
    ));

    listaMenu.add(
      new OpcoesMenu(
        image: 'assets/images/pessoais.png',
        titulo: 'Orações',
        ordem: 5,
        destino: 'oracoes_comuns',
        grupo: 0,
        subtitulo: '.. ',
      ),
    );
  }
}
