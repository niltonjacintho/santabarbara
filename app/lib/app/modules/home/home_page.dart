import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/velavirtual/velavirtual_controller.dart';
import 'package:santabarbara/app/pages/slide_bar/slide_bar_page.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller
  UtilsController utils = Modular.get();
  AuthController authController = Modular.get();
  VelavirtualController velavirtual = Modular.get();
  final _scrollController = FixedExtentScrollController();
  int _index = 0;

  @override
  void initState() {
    super.initState();
    velavirtual.getVelasAcesas(authController.user.uid);
  }

  @override
  Widget build(BuildContext context) {
    controller.fillMenu();
    return WillPopScope(
      onWillPop: () async {
        UtilsController().showLongToast(
            'Muito obrigado por ter vindo! \nVolte sempre!\bA casa é sua!');
        SystemNavigator.pop();
        //  return Future<true>();
        return false;
      },
      child: new Scaffold(
        backgroundColor: utils.corFundoAvisos,
        appBar: new AppBar(
          title: new Text('Sua Paróquia'),
          backgroundColor: Colors.red[900],
        ),
        floatingActionButton: Observer(
          builder: (_) => Opacity(
            opacity: velavirtual.qtdVelasAcesas < 1
                ? 0
                : 1, // visible if showShould is true
            child: FloatingActionButton.extended(
              onPressed: () {
                Modular.to.pushNamed('/vela');
                // Add your onPressed code here!
              },
              label: Text(
                  'você possui ${velavirtual.velasAcesas.length} vela(s) acesas'),
              icon: Icon(Icons.thumb_up),
              backgroundColor: Colors.pink,
            ),
          ),
        ),
        drawer: AppDrawer(),
        body: GestureDetector(
          child: ListWheelScrollView.useDelegate(
            onSelectedItemChanged: (value) => {
              setState(() {
                _index = value;
                print(_index);
              })
            },
            controller: _scrollController,
            itemExtent: 200,
            childDelegate: ListWheelChildBuilderDelegate(
              childCount: controller.listaMenu.length,
              builder: (context, index) => Container(
                color: controller.listaMenu[index].cardColor != null
                    ? controller.listaMenu[index].cardColor
                    : Colors.white,
                child: ListTile(
                  leading: Image.asset(controller.listaMenu[index].image),
                  title: Text(
                    controller.listaMenu[index].titulo,
                    style: TextStyle(fontSize: 36),
                  ),
                  subtitle: Text(
                    controller.listaMenu[index].subtitulo,
                    style: TextStyle(
                      color: controller.listaMenu[index].textColor != null
                          ? controller.listaMenu[index].textColor
                          : Colors.white,
                      fontSize: 22,
                    ),
                  ),
                  onTap: () => {
                    print(index),
                  },
                ),
              ),
            ),
          ),
          onTap: () => {
            controller.gotoDestino(
                controller.listaMenu[_index].destino, context),
            print(_index),
          },
        ),
      ),
    );
  }
}
