import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart' show ByteData, rootBundle;
import 'package:path_provider/path_provider.dart';
import 'package:santabarbara/app/models/agenda_atividade_model.dart';
import 'package:santabarbara/app/modules/agenda/repositories/iagenda_repository_interface.dart';

class AgendaRepository implements IAgendaRepository {
  List<String> lines;
  List<AgendaAtividade> listAtividades = [];
  AgendaAtividade agendaAtividade = new AgendaAtividade('', '', '', '', '');
  @override
  changeAgendaView() {
    throw UnimplementedError();
  }

  @override
  getApointments() {
    throw UnimplementedError();
  }

  Future<List<AgendaAtividade>> getDay(DateTime day) async {
    List<AgendaAtividade> agenda = new List<AgendaAtividade>();

    await FirebaseFirestore.instance
        .collection('agenda')
        .where('data', isEqualTo: day.toString().substring(0, 10))
        .get()
        .then((snapshot) {
      snapshot.docs.forEach((element) {
        agenda.add(new AgendaAtividade(
            element.get('data'),
            element.get('atividade'),
            element.get('pastoral'),
            element.get('hora'),
            element.get('local')));
      });
    });
    return agenda;
  }

  @override
  Future<Map<DateTime, List>> getEvents() async {
    Map<DateTime, List> map = new Map();
    await FirebaseFirestore.instance.collection('agenda').orderBy('data').get()
        //.where('data', ise)
        .then(
      (snapshot) {
        for (DocumentSnapshot ds in snapshot.docs) {
          List atividades = [];
          if (ds.get('data') != null && ds.get('atividade') != null) {
            atividades = ds.get('atividade').toString().split('/');
            map.putIfAbsent(
                DateTime.parse(ds.get('data')), () => atividades);
          }
        }
      },
    );
    return map;
  }

  @override
  getFirstDayOfWeek() {
    throw UnimplementedError();
  }

  @override
  getHolidays() {
    throw UnimplementedError();
  }

  @override
  uploadEventos() async {
    var futureContent = await getFileData('assets/data/agenda2020.csv');
    futureContent.forEach((c) {
      lines = c.split(",");
      if (lines[0] != 'data') {
        try {
          lines[4] = lines[4];
        } catch (e) {
          lines.add('');
        }
        agendaAtividade = new AgendaAtividade(
            lines[0], lines[1], lines[2], lines[3], lines[4]);
        listAtividades.add(agendaAtividade);
        _upload('agenda', agendaAtividade);
      }
      //})
    }); // (3)
  }

  @override
  uploadHolidays() {
    throw UnimplementedError();
  }

  cleanAgenda() async {
    // await Firestore.instance.collection('agenda').get().then(
    //   (snapshot) {
    //     for (DocumentSnapshot ds in snapshot.documents) {
    //       ds.reference.delete();
    //     }
    //   },
    // );
  }

  Future<bool> _upload(String grupo, AgendaAtividade obj) async {
    var result = false;
    await FirebaseFirestore.instance
        .collection("agenda")
        .add(obj.toMap())
        .then((res) => {result = true})
        .catchError((err) => result = false);
    return result;
  }

  Future<List<String>> getFileData(String path) async {
    ByteData data = await rootBundle.load(path);
    String directory = (await getTemporaryDirectory()).path;
    File file = await writeToFile(data, '$directory/bot.txt');
    var linhas = await file.readAsLines();
    return linhas;
  }

  Future<File> writeToFile(ByteData data, String path) {
    ByteBuffer buffer = data.buffer;
    return File(path).writeAsBytes(buffer.asUint8List(
      data.offsetInBytes,
      data.lengthInBytes,
    ));
  }
}
