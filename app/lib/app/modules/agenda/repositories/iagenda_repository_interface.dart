abstract class IAgendaRepository {
  getEvents();
  getHolidays();
  getFirstDayOfWeek();
  getApointments();
  changeAgendaView();
  uploadEventos();
  uploadHolidays();
}
