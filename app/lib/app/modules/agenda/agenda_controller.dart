import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/models/agenda_atividade_model.dart';
import 'package:santabarbara/app/modules/agenda/repositories/agenda_repository.dart';

part 'agenda_controller.g.dart';

class AgendaController = _AgendaControllerBase with _$AgendaController;

abstract class _AgendaControllerBase with Store {
  final AgendaRepository agendaRepository = new AgendaRepository();
  @observable
  Map<DateTime, List> _eventos;

  @observable
  DateTime _diaSelecionado;

  @observable
  AgendaAtividade _agendaDia;

  set setagendaDia(AgendaAtividade value) => _agendaDia = value;
  AgendaAtividade get agendaDia => _agendaDia;

  set setdiaSelecionado(DateTime value) => _diaSelecionado = value;
  get diaSelecionado => _diaSelecionado;

  set seteventos(Map<DateTime, List> value) => _eventos = value;
  get eventos => _eventos;

  @action
  loadData() {
    agendaRepository.uploadEventos();
  }

  @action
  Future<Map<DateTime, List>> getEvents() async {
    return await agendaRepository
        .getEvents()
        .then((Map<DateTime, List> lista) => this.seteventos = lista);
  }

  @action
  cleanData() {
    agendaRepository.cleanAgenda();
    //  return Future<String
  }

  @action
  Future<List<AgendaAtividade>> getDayValue(DateTime day) async {
    List<AgendaAtividade> agenda;
    await agendaRepository.getDay(day).then((dados) => {
          //
          ('dados no formato agenda retornados do repositorio $dados'),
          agenda = dados,
          _agendaDia = agenda[0],
        });
    return agenda;
  }
}
