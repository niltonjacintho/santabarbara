// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agenda_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AgendaController on _AgendaControllerBase, Store {
  final _$_eventosAtom = Atom(name: '_AgendaControllerBase._eventos');

  @override
  Map<DateTime, List<dynamic>> get _eventos {
    _$_eventosAtom.reportRead();
    return super._eventos;
  }

  @override
  set _eventos(Map<DateTime, List<dynamic>> value) {
    _$_eventosAtom.reportWrite(value, super._eventos, () {
      super._eventos = value;
    });
  }

  final _$_diaSelecionadoAtom =
      Atom(name: '_AgendaControllerBase._diaSelecionado');

  @override
  DateTime get _diaSelecionado {
    _$_diaSelecionadoAtom.reportRead();
    return super._diaSelecionado;
  }

  @override
  set _diaSelecionado(DateTime value) {
    _$_diaSelecionadoAtom.reportWrite(value, super._diaSelecionado, () {
      super._diaSelecionado = value;
    });
  }

  final _$_agendaDiaAtom = Atom(name: '_AgendaControllerBase._agendaDia');

  @override
  AgendaAtividade get _agendaDia {
    _$_agendaDiaAtom.reportRead();
    return super._agendaDia;
  }

  @override
  set _agendaDia(AgendaAtividade value) {
    _$_agendaDiaAtom.reportWrite(value, super._agendaDia, () {
      super._agendaDia = value;
    });
  }

  final _$getEventsAsyncAction = AsyncAction('_AgendaControllerBase.getEvents');

  @override
  Future<Map<DateTime, List<dynamic>>> getEvents() {
    return _$getEventsAsyncAction.run(() => super.getEvents());
  }

  final _$getDayValueAsyncAction =
      AsyncAction('_AgendaControllerBase.getDayValue');

  @override
  Future<List<AgendaAtividade>> getDayValue(DateTime day) {
    return _$getDayValueAsyncAction.run(() => super.getDayValue(day));
  }

  final _$_AgendaControllerBaseActionController =
      ActionController(name: '_AgendaControllerBase');

  @override
  dynamic loadData() {
    final _$actionInfo = _$_AgendaControllerBaseActionController.startAction(
        name: '_AgendaControllerBase.loadData');
    try {
      return super.loadData();
    } finally {
      _$_AgendaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic cleanData() {
    final _$actionInfo = _$_AgendaControllerBaseActionController.startAction(
        name: '_AgendaControllerBase.cleanData');
    try {
      return super.cleanData();
    } finally {
      _$_AgendaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
