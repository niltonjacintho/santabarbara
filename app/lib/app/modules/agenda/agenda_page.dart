import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'agenda_controller.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:slide_popup_dialog/slide_popup_dialog.dart' as slideDialog;

class AgendaPage extends StatefulWidget {
  final String title;
  const AgendaPage({Key key, this.title = "Agenda"}) : super(key: key);

  @override
  _AgendaPageState createState() => _AgendaPageState();
}

class _AgendaPageState extends ModularState<AgendaPage, AgendaController> {
  // AnimationController _animationController;
  // CalendarController _calendarController;
  AgendaController agendaController = Modular.get();

  UtilsController utilControle = Modular.get();
  @override
  void initState() {
    super.initState();
    // _calendarController = CalendarController();
  }

  @override
  void dispose() {
    // _calendarController.dispose();
    super.dispose();
  }

  Container gerarTexto(String label, dynamic valor) {
    return Container(
      child: Observer(
        builder: (_) => TextFormField(
          decoration: InputDecoration(
            labelText: label,
            contentPadding: EdgeInsets.all(10),
            border: InputBorder.none,
          ),
          style: new TextStyle(fontSize: 22),
          initialValue: valor,
          enabled: false,
        ),
      ),
    );
  }

  // ignore: unused_element
  void _onDaySelected(DateTime day, List events) {
    agendaController.getDayValue(day);
    Future.delayed(
      const Duration(milliseconds: 500),
      () {
        slideDialog.showSlideDialog(
          context: context,
          backgroundColor: Colors.blueGrey,
          pillColor: Colors.amber,
          child: Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Text(
                  'Eventos do dia ${day.day}/${day.month}',
                  style: TextStyle(fontSize: 30),
                ),
                FutureBuilder(
                    future: agendaController.getDayValue(day),
                    initialData: [],
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting)
                        return Center(child: CircularProgressIndicator());
                      else if (snapshot.hasData) {
                        if (snapshot.data != null) {
                          return listarEventos(context, snapshot);
                        } else {
                          return Align(
                            alignment: Alignment.center,
                            child: Text('Não existem eventos neste dia'),
                          );
                        }
                      } else if (snapshot.hasError)
                        return Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Não existem eventos neste dia',
                            style:
                                TextStyle(fontSize: 25, color: Colors.red[900]),
                          ),
                        );
                      else
                        return Text('None');
                    }),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget listarEventos(BuildContext context, AsyncSnapshot snapshot) {
    return Expanded(
      child: ListView.builder(
        itemCount: snapshot.data.length,
        itemBuilder: (context, i) {
          return Padding(
            padding: EdgeInsets.only(left: 5, right: 5, bottom: 10),
            child: Card(
              elevation: 8,
              child: ListTile(
                trailing:
                    Text(snapshot.data[i].hora, style: TextStyle(fontSize: 20)),
                title: Text(snapshot.data[i].atividade,
                    style: TextStyle(fontSize: 20)),
                subtitle: Text(
                  snapshot.data[i].pastoral != ''
                      ? '${snapshot.data[i].pastoral.toString().trim()}\n${snapshot.data[i].local}'
                      : snapshot.data[i].local,
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    agendaController.getEvents();
    UtilsController utilsController = Modular.get();
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Agenda Paroquial'),
      ),
      body: SafeArea(
        left: false,
        top: true,
        right: false,
        bottom: true,
        child: Observer(
          builder: (_) => Text('Em desenvolvimento') 
          // TableCalendar(
          //   events: agendaController.eventos,
          //   locale: utilsController.defaultLanguage,
          //   calendarController: _calendarController,
          //   calendarStyle: CalendarStyle(
          //     weekdayStyle: TextStyle(
          //       fontSize: 18,
          //       color: Colors.blue[900],
          //     ),
          //     selectedStyle: TextStyle(
          //         fontSize: 18,
          //         color: Colors.blue[900],
          //         fontWeight: FontWeight.bold),
          //     weekendStyle: TextStyle(
          //         fontSize: 18,
          //         color: Colors.red[900],
          //         fontWeight: FontWeight.bold),
          //     selectedColor: Colors.yellow[100],
          //     todayColor: Colors.blue[900],
          //     markersColor: Colors.brown[700],
          //     outsideDaysVisible: false,
          //   ),
          //   daysOfWeekStyle: DaysOfWeekStyle(
          //       weekendStyle: TextStyle(fontSize: 20),
          //       weekdayStyle: TextStyle(fontSize: 18)),
          //   headerStyle: HeaderStyle(
          //     titleTextStyle: TextStyle(
          //         fontSize: 20,
          //         color: Colors.red[900],
          //         fontWeight: FontWeight.bold),
          //     formatButtonTextStyle:
          //         TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
          //     formatButtonDecoration: BoxDecoration(
          //       color: Colors.deepOrange[400],
          //       borderRadius: BorderRadius.circular(16.0),
          //     ),
          //   ),
          //   // onDaySelected:
          //   //     _onDaySelected, //(day, events) agendaController.onDaySelected,
          // ),
        ),
      ),
    );
  }
}
