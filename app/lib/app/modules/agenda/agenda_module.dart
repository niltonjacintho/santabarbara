import 'package:santabarbara/app/modules/agenda/agenda_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/agenda/agenda_page.dart';

class AgendaModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => AgendaController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => AgendaPage()),
      ];

  static Inject get to => Inject<AgendaModule>(); //>.of(;
}
