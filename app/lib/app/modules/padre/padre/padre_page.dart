import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'padre_controller.dart';

class PadrePage extends StatefulWidget {
  final String title;
  const PadrePage({Key key, this.title = "Padre"}) : super(key: key);

  @override
  _PadrePageState createState() => _PadrePageState();
}

class _PadrePageState extends ModularState<PadrePage, PadreController> {
  //use 'controller' variable to access controller
  int _index = 0;
  PadreController padreController = Modular.get();
  UtilsController utils = Modular.get();
  // GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  @override
  Widget build(BuildContext context) {
    Stream<QuerySnapshot> myStream = padreController.getList();
    //pa.getimages;
    return Scaffold(
      backgroundColor: utils.corFundoAvisos,
      //appBar: AppBar(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.pop();
          }),
      body: StreamBuilder<QuerySnapshot>(
          stream: myStream,
          builder: (context, snapshot) {
            List<Widget> children;
            if (snapshot.hasError) {
              children = <Widget>[
                Icon(
                  Icons.error_outline,
                  color: Colors.red,
                  size: 60,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Text(
                      'Erro ao recuperar a informação \ndos párocos: ${snapshot.error}'),
                )
              ];
            } else {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  children = <Widget>[
                    Icon(
                      Icons.info,
                      color: Colors.blue,
                      size: 60,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Center(
                        child: Text(
                            'Não encontrei informações /ndos nossos párocos'),
                      ),
                    ),
                  ];
                  break;
                case ConnectionState.waiting:
                  return Column(children: <Widget>[
                    SizedBox(
                      child: const CircularProgressIndicator(),
                      width: 60,
                      height: 60,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text('Recuperando informações.'),
                    )
                  ]);
                  break;
                case ConnectionState.done:
                  return Column(children: <Widget>[
                    Icon(
                      Icons.info,
                      color: Colors.blue,
                      size: 60,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('\$${snapshot.data} (closed)'),
                    )
                  ]);
                  break;
                case ConnectionState.active:
                  final int messageCount = snapshot.data.docs.length;
                  padreController.loadKeys(messageCount);
                  return Center(
                    child: SizedBox(
                      height: 500, // card height
                      child: PageView.builder(
                        itemCount: messageCount,
                        controller: PageController(viewportFraction: 0.7),
                        onPageChanged: (int index) =>
                            setState(() => _index = index),
                        itemBuilder: (_, i) {
                          return Transform.scale(
                            scale: i == _index ? 1 : 0.9,
                            child: Card(
                              elevation: 6,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              child: FlipCard(
                                key: padreController.listKey[i],
                                flipOnTouch: false,
                                front: Container(
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: 400,
                                        child: GestureDetector(
                                          onTap: () => {
                                            padreController
                                                .listKey[i].currentState
                                                .toggleCard(),
                                          },
                                          child: snapshot.data.docs[i]
                                                      .get('imagem') !=
                                                  ''
                                              ? Image.network(snapshot
                                                  .data.docs[i]
                                                  .get('imagem'))
                                              : Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 46),
                                                  child: Text(
                                                    'Ops. \nNão achei a imagem do pároco',
                                                    style: TextStyle(
                                                      fontSize: 30,
                                                      // textAlign:
                                                      //     TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Column(
                                          children: <Widget>[
                                            Text(
                                              snapshot.data.docs[i]
                                                  .get('titulo'),
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              snapshot.data.docs[i]
                                                  .get('subtitulo'),
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                      //    ),
                                    ],
                                  ),
                                ),
                                back: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: GestureDetector(
                                    onTap: () {
                                      padreController.listKey[i].currentState
                                          .toggleCard();
                                    },
                                    child: Text(snapshot.data.docs[i]
                                        .get('conteudo')),
                                  ),
                                ),
                              ),
                            ),
                          );
                          //);
                        },
                      ),
                    ),
                  );
                  break;
              }
            }
          }),
    );
  }
}
