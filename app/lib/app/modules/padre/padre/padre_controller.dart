import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'padre_controller.g.dart';

class PadreController = _PadreControllerBase with _$PadreController;

abstract class _PadreControllerBase with Store {
  @observable
  List<String> _images;
  List<String> get images => _images;

  // set images(List<String> value) => _images = value;

  List<String> get getimages => _images;
  @observable
  List<GlobalKey<FlipCardState>> listKey;

  loadKeys(int qtd) {
    listKey = List<GlobalKey<FlipCardState>>();
    for (var i = 0; i < qtd; i++) {
      listKey.add(GlobalKey<FlipCardState>());
    }
  }

  set setimages(List<String> value) => _images = value;

  Stream<QuerySnapshot> getList() {
    var x = FirebaseFirestore.instance
        .collection("artigos")
        //.where('ativo', isEqualTo: true)
        .where('grupo', isEqualTo: 'padres')
        .snapshots();
    //loadKeys(x.length);
    return x;
  }
}
