// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'padre_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PadreController on _PadreControllerBase, Store {
  final _$_imagesAtom = Atom(name: '_PadreControllerBase._images');

  @override
  List<String> get _images {
    _$_imagesAtom.reportRead();
    return super._images;
  }

  @override
  set _images(List<String> value) {
    _$_imagesAtom.reportWrite(value, super._images, () {
      super._images = value;
    });
  }

  final _$listKeyAtom = Atom(name: '_PadreControllerBase.listKey');

  @override
  List<GlobalKey<FlipCardState>> get listKey {
    _$listKeyAtom.reportRead();
    return super.listKey;
  }

  @override
  set listKey(List<GlobalKey<FlipCardState>> value) {
    _$listKeyAtom.reportWrite(value, super.listKey, () {
      super.listKey = value;
    });
  }

  @override
  String toString() {
    return '''
listKey: ${listKey}
    ''';
  }
}
