import 'package:santabarbara/app/modules/padre/padre/padre_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/padre/padre/padre_page.dart';

class PadreModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PadreController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute, child: (_, args) => PadrePage()),
      ];

  static Inject get to => Inject<PadreModule>(); //>.of(;
}
