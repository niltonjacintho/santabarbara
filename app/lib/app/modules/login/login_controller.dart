import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  AuthController authController = Modular.get();

  @observable
  bool loading = false;

  @action
  Future loginWithGoole() async {
    try {
      loading = true;
      await authController.loginWithGoogle();
      Modular.to.pushNamed('/home');
    } catch (e) {
      loading = false;
    }
  }

  @action
  Future loginWithFAcebook() async {
    try {
      loading = true;
      await authController.loginWithFacebook();
      print(authController.userFace);
    } catch (e) {
      loading = false;
    }
  }
}
