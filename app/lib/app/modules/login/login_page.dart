import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
// import 'package:santabarbara/app/modules/remedios/remedios_controller.dart';
import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  LoginController controlerLogin = new LoginController();

  @override
  Widget build(BuildContext context) {
    // NÃO APAGAR - RemediosController remedios_controller = Modular.get();
    // var url =
    //     "https://www.arqrio.com.br/curia/ajaxParoquiasRecuperarDetalhes.php?id=265";
    // ignore: unused_local_variable
    Dio dio;
    // Response response;
    return Scaffold(
      backgroundColor: Color.fromRGBO(179, 45, 0, 1.0),
      body: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding:
                  EdgeInsets.only(top: 50, left: 10, right: 10, bottom: 50),
              child: Image.asset('assets/images/logonegro.png',
                  height: 180, width: 180),
            ),
          ),
          Container(
            width: 300,
            child: new RaisedButton(
              elevation: 0.0,
              child: ListTile(
                leading: Image.asset('assets/images/google.png'),
                title: Text(
                  'Google Login',
                  style: TextStyle(fontSize: 20),
                ),
                //    trailing: Icon(Icons.more_vert),
              ),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              padding:
                  EdgeInsets.only(top: 7.0, bottom: 7.0, right: 7.0, left: 7.0),
              onPressed: () => {
                controlerLogin.loginWithGoole(),
              },
            ),
          ),
          SizedBox(height: 20),
          Container(
            width: 300,
            child: new RaisedButton(
              elevation: 0.0,
              child: ListTile(
                leading: Image.asset('assets/images/facebook.png'),
                title: Text(
                  'Facebook Login',
                  style: TextStyle(fontSize: 20),
                ),
              ),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              padding:
                  EdgeInsets.only(top: 7.0, bottom: 7.0, right: 7.0, left: 7.0),
              onPressed: () async => {
                // desativado mas funcionando     remedios_controller.montarListaRemedios(),
                // await remedios_controller.gravarRemedios(),
                controlerLogin.loginWithFAcebook(),
                // Alert(
                //         type: AlertType.warning,
                //         context: context,
                //         title: "Desativado",
                //         desc:
                //             "O processo de autenticação pelo Facebook esta em desenvolvimento! Avisaremos quando o mesmo for liberado!")
                //     .show(),
                // controlerLogin.loginWithFAcebook(),
              },
            ),
          ),
        ],
      ),
    );
  }
}
