import 'package:mobx/mobx.dart';

part 'perguntas_controller.g.dart';

class PerguntasController = _PerguntasControllerBase with _$PerguntasController;

abstract class _PerguntasControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
