import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/modules/perguntas/perguntas_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/perguntas/perguntas_page.dart';

class PerguntasModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PerguntasController()),
        Bind((i) => DocumentsController()),
      ];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => PerguntasPage()),
      ];

  static Inject get to => Inject<PerguntasModule>(); //>.of(;
}
