import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';
import 'perguntas_controller.dart';

class PerguntasPage extends StatefulWidget {
  final String title;
  const PerguntasPage({Key key, this.title = "Perguntas"}) : super(key: key);

  @override
  _PerguntasPageState createState() => _PerguntasPageState();
}

class _PerguntasPageState
    extends ModularState<PerguntasPage, PerguntasController> {
  UtilsController util = Modular.get();
  DocumentsController documentsController = Modular.get();

  @override
  Widget build(BuildContext context) {
    Stream<QuerySnapshot> myStream =
        documentsController.getStreamByGroup(util.grupoPerguntas.id);
    return Scaffold(
      body: Container(
        color: util.corFundoAvisos,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text('Perguntas frequentes'),
              backgroundColor: Colors.green,
              expandedHeight: 200,
              floating: true,
              pinned: true,
              snap: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Image.asset(
                  'assets/images/perguntasfrequentes.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            StreamBuilder(
              stream: myStream,
              builder: (context, snapshot) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      if (!snapshot.hasData) {
                        return Text("Loading..");
                      }
                      return Padding(
                        padding: const EdgeInsets.all(
                            10), // .symmetric(horizontal: 12.0),
                        child: ExpansionTileCard(
                          // leading:
                          //     CircleAvatar(child: Text('${myStream. .data()['titulo']}')),
                          title: Text(
                            snapshot.data.docs[index].data()['titulo'],
                            style: new TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text('Resposta!'),
                          children: <Widget>[
                            Divider(
                              thickness: 1.0,
                              height: 1.0,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                  vertical: 8.0,
                                ),
                                child: Text(
                                  snapshot.data.docs[index].data()['conteudo'],
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .copyWith(fontSize: 16),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                      //   },
                      //   );
                    },
                    childCount:
                        snapshot.hasData ? snapshot.data.docs.length : 0,
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
