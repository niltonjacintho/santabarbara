// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'utils_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UtilsController on _UtilsControllerBase, Store {
  final _$corAtom = Atom(name: '_UtilsControllerBase.cor');

  @override
  Color get cor {
    _$corAtom.reportRead();
    return super.cor;
  }

  @override
  set cor(Color value) {
    _$corAtom.reportWrite(value, super.cor, () {
      super.cor = value;
    });
  }

  final _$grupoAtivoAtom = Atom(name: '_UtilsControllerBase.grupoAtivo');

  @override
  Grupos get grupoAtivo {
    _$grupoAtivoAtom.reportRead();
    return super.grupoAtivo;
  }

  @override
  set grupoAtivo(Grupos value) {
    _$grupoAtivoAtom.reportWrite(value, super.grupoAtivo, () {
      super.grupoAtivo = value;
    });
  }

  final _$_UtilsControllerBaseActionController =
      ActionController(name: '_UtilsControllerBase');

  @override
  List<DropdownMenuItem<dynamic>> getCategoryList() {
    final _$actionInfo = _$_UtilsControllerBaseActionController.startAction(
        name: '_UtilsControllerBase.getCategoryList');
    try {
      return super.getCategoryList();
    } finally {
      _$_UtilsControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cor: ${cor},
grupoAtivo: ${grupoAtivo}
    ''';
  }
}
