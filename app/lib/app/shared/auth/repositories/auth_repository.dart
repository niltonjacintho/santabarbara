import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class AuthRepository implements IAuthRepository {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final facebookLogin = FacebookLogin();
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  @override
  Future<User> getEmailLogin() async {
    return {} as User;
  }

  @override
  Future getFacebookLogin() async {
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        _showMessage('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        break;
      case FacebookLoginStatus.cancelledByUser:
        _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        _showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  // Future<Null> _logOut() async {
  //   await facebookSignIn.logOut();
  //   _showMessage('Logged out.');
  // }

  void _showMessage(String message) {
  //  print(message);
  }

  @override
  Future<User> getGoogleLogin() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    AuthController authController = Modular.get();
    
    authController.setuserGoogle(_googleSignIn);
    final User user =
        (await _auth.signInWithCredential(credential)).user;
    // print("signed in " + user.displayName);
    // print(user);
    return user;
  }

  @override
  Future<String> getToken() {
    return null;
  }

  @override
  User getUser() {
    return FirebaseAuth.instance.currentUser;
  }
}
