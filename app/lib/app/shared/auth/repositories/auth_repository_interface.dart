import 'package:firebase_auth/firebase_auth.dart';

abstract class IAuthRepository {
  User getUser();
  Future<String> getToken();
  Future<User> getGoogleLogin();
  Future getEmailLogin();
  Future getFacebookLogin();
}
