// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AuthController on _AuthControllerBase, Store {
  final _$_checkingAtom = Atom(name: '_AuthControllerBase._checking');

  @override
  bool get _checking {
    _$_checkingAtom.reportRead();
    return super._checking;
  }

  @override
  set _checking(bool value) {
    _$_checkingAtom.reportWrite(value, super._checking, () {
      super._checking = value;
    });
  }

  final _$userAtom = Atom(name: '_AuthControllerBase.user');

  @override
  User get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$userGoogleAtom = Atom(name: '_AuthControllerBase.userGoogle');

  @override
  GoogleSignIn get userGoogle {
    _$userGoogleAtom.reportRead();
    return super.userGoogle;
  }

  @override
  set userGoogle(GoogleSignIn value) {
    _$userGoogleAtom.reportWrite(value, super.userGoogle, () {
      super.userGoogle = value;
    });
  }

  final _$userFaceAtom = Atom(name: '_AuthControllerBase.userFace');

  @override
  UserCredential get userFace {
    _$userFaceAtom.reportRead();
    return super.userFace;
  }

  @override
  set userFace(UserCredential value) {
    _$userFaceAtom.reportWrite(value, super.userFace, () {
      super.userFace = value;
    });
  }

  final _$loginWithGoogleAsyncAction =
      AsyncAction('_AuthControllerBase.loginWithGoogle');

  @override
  Future<dynamic> loginWithGoogle() {
    return _$loginWithGoogleAsyncAction.run(() => super.loginWithGoogle());
  }

  final _$loginWithFacebookAsyncAction =
      AsyncAction('_AuthControllerBase.loginWithFacebook');

  @override
  Future<dynamic> loginWithFacebook() {
    return _$loginWithFacebookAsyncAction.run(() => super.loginWithFacebook());
  }

  final _$_AuthControllerBaseActionController =
      ActionController(name: '_AuthControllerBase');

  @override
  dynamic setuserGoogle(GoogleSignIn value) {
    final _$actionInfo = _$_AuthControllerBaseActionController.startAction(
        name: '_AuthControllerBase.setuserGoogle');
    try {
      return super.setuserGoogle(value);
    } finally {
      _$_AuthControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setUser(User value) {
    final _$actionInfo = _$_AuthControllerBaseActionController.startAction(
        name: '_AuthControllerBase.setUser');
    try {
      return super.setUser(value);
    } finally {
      _$_AuthControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
user: ${user},
userGoogle: ${userGoogle},
userFace: ${userFace}
    ''';
  }
}
