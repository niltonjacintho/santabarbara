import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:santabarbara/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:google_sign_in/google_sign_in.dart';

part 'auth_controller.g.dart';

class AuthController = _AuthControllerBase with _$AuthController;

abstract class _AuthControllerBase with Store {
  final IAuthRepository _authrepository = Modular.get();
 // Map<String, dynamic> _userData;
 // AccessToken _accessToken;

  @observable
  bool _checking = true;

  @observable
  User user;

  @observable
  GoogleSignIn userGoogle;

  @action
  setuserGoogle(GoogleSignIn value) => userGoogle = value;

  GoogleSignIn get getuserGoogle => userGoogle;

  @observable
  UserCredential userFace;

  @action
  setUser(User value) => user = value;

  _AuthControllerBase() {
    _authrepository.getUser();
  }

  @action
  Future loginWithGoogle() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final GoogleAuthCredential googleAuthCredential =
        GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    var userCredential = await _auth.signInWithCredential(googleAuthCredential);
    if (userCredential?.user != null) {
      user = userCredential.user;
    } else {
      user = null;
    }
  }

  @action
  Future loginWithFacebook() async {
    userFace = await signInWithFacebook();
    // _authrepository.getFacebookLogin();
  }

  Future<UserCredential> signInWithFacebook() async {
    // Trigger the sign-in flow
    final  result = await FacebookAuth.instance.login();

    // Create a credential from the access token
    final FacebookAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(result.accessToken.token);

    // Once signed in, return the UserCredential
    var ret;
    try {
      ret = await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
    } catch (e) {
      print(e);
    }
    return ret;
  }

  // Future<void> _checkIfIsLogged() async {
  //   final AccessToken accessToken = await FacebookAuth.instance.isLogged;
  //   //   setState(() {
  //   _checking = false;
  //   //   });
  //   if (accessToken != null) {
  //     //  print("is Logged:::: ${prettyPrint(accessToken.toJson())}");
  //     // now you can call to  FacebookAuth.instance.getUserData();
  //  //   final userData = await FacebookAuth.instance.getUserData();
  //     // final userData = await FacebookAuth.instance.getUserData(fields: "email,birthday,friends,gender,link");
  //  //   _accessToken = accessToken;
  //     //   setState(() {
  //   //  _userData = userData;
  //     //   });
  //   }
 // }
}
