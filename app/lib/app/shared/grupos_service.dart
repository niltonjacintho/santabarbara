import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/grupos_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:santabarbara/app/store/base_store.dart';
import 'package:mobx/mobx.dart';

// import 'package:mobx/mobx.dart';

// part 'grupos_service.g.dart';

class GruposService extends Disposable {
  //dispose will be called automatically
  @override
  void dispose() {}

  @observable
  BaseStore store = Modular.get();

  GruposService() {
    getGroupList(null).then((value) => {
          for (var item in value)
            {
              print('ENTREIIIIII '),
              _lista.add(item),
            }
        });
  }

  @observable
  List<Grupos> _lista = [];

  Future<List<Grupos>> getGroupList(String tipo) async {
    if (store.lista.length <= 0) {
      List<Grupos> lista = [];
      var snap;
      if (tipo != null) {
        snap = FirebaseFirestore.instance
            .collection('grupos')
            .orderBy("id")
            .where('tipos ', isEqualTo: tipo)
            .snapshots();
      } else {
        snap = FirebaseFirestore.instance
            .collection('grupos')
            .orderBy("id")
            .snapshots();
        // .then((artigos) async {
        //   return await Future.forEach(artigos.docs)
        // .then((onValue)
        // }
      }
// await
      try {
        store.lista = [];
        await snap.forEach((field) {
          field.docs.asMap().forEach((index, data) {
            try {
              Grupos g = new Grupos();
              g.id = data['id'];
              g.cor = data['cor'];
              g.titulo = data['titulo'];
              g.value = data['value'];
              lista.add(g);
              _lista.add(g);
              store.lista.add(g);
            } catch (e) {
              print('error $e');
            }
          });
        });
        _lista = lista;
        store.lista = lista;
      } catch (e) {
        print(e);
      }
      return lista;
    }
    return store.lista;
  }

  Grupos getGroupById(String id) {
    Grupos g = new Grupos();
    g = store.lista.where((element) => element.id == id).first;
    return g;
  }

  Grupos generateGroupObject(dynamic data) {
    Grupos g = new Grupos();
    g.id = data['id'];
    g.cor = data['cor'];
    g.titulo = data['titulo'];
    g.value = data['value'];
    return g;
  }
}
