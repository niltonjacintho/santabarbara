import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/paroquias/capelas/capelas_page.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_page.dart';

class ParoquiasModule extends Module {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRoute> get routers => [
        ChildRoute(Modular.initialRoute,
            child: (_, args) => ParoquiasPage()),
        ChildRoute('/capelas', child: (_, args) => CapelasPage()),
      ];

  static Inject get to => Inject<ParoquiasModule>(); //>.of(;
}
