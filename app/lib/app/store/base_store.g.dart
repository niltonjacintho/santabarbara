// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BaseStore on _BaseStoreBase, Store {
  final _$listaAtom = Atom(name: '_BaseStoreBase.lista');

  @override
  List<Grupos> get lista {
    _$listaAtom.reportRead();
    return super.lista;
  }

  @override
  set lista(List<Grupos> value) {
    _$listaAtom.reportWrite(value, super.lista, () {
      super.lista = value;
    });
  }

  final _$oracaoAtom = Atom(name: '_BaseStoreBase.oracao');

  @override
  Oracoes get oracao {
    _$oracaoAtom.reportRead();
    return super.oracao;
  }

  @override
  set oracao(Oracoes value) {
    _$oracaoAtom.reportWrite(value, super.oracao, () {
      super.oracao = value;
    });
  }

  @override
  String toString() {
    return '''
lista: ${lista},
oracao: ${oracao}
    ''';
  }
}
