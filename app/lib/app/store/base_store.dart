import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/models/grupos_model.dart';
import 'package:santabarbara/app/models/oracao_model.dart';

part 'base_store.g.dart';

@Injectable()
class BaseStore = _BaseStoreBase with _$BaseStore;

abstract class _BaseStoreBase with Store {
  @observable
  List<Grupos> lista = [];

  @observable
  Oracoes oracao;
  
}
