import 'dart:convert';

class AgendaAtividade {
  final String data;
  final String atividade;
  final String pastoral;
  final String hora;
  final String local;
  AgendaAtividade(
    this.data,
    this.atividade,
    this.pastoral,
    this.hora,
    this.local,
  );

  // AgendaAtividade copyWith({
  //   String data,
  //   String atividade,
  //   String pastoral,
  //   String hora,
  //   String local,
  // }) {
  //   return AgendaAtividade(
  //     data: data ?? this.data,
  //     atividade: atividade ?? this.atividade,
  //     pastoral: pastoral ?? this.pastoral,
  //     hora: hora ?? this.hora,
  //     local: local ?? this.local,
  //   );
  // }

  Map<String, dynamic> toMap() {
    return {
      'data': data,
      'atividade': atividade,
      'pastoral': pastoral,
      'hora': hora,
      'local': local,
    };
  }

  Map<String, dynamic> toMapEvent() {
    return {
      'data': data,
      'atividade': atividade,
      'pastoral': pastoral,
      'hora': hora,
      'local': local,
    };
  }

  // static AgendaAtividade fromMap(Map<String, dynamic> map) {
  //   if (map == null) return null;

  //   return AgendaAtividade(
  //     data: map['data'],
  //     atividade: map['atividade'],
  //     pastoral: map['pastoral'],
  //     hora: map['hora'],
  //     local: map['local'],
  //   );
  // }

  String toJson() => json.encode(toMap());

  // static AgendaAtividade fromJson(String source) =>
  //     fromMap(json.decode(source));

  @override
  String toString() {
    return 'AgendaAtividade(data: $data, atividade: $atividade, pastoral: $pastoral, hora: $hora, local: $local)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is AgendaAtividade &&
        o.data == data &&
        o.atividade == atividade &&
        o.pastoral == pastoral &&
        o.hora == hora &&
        o.local == local;
  }

  @override
  int get hashCode {
    return data.hashCode ^
        atividade.hashCode ^
        pastoral.hashCode ^
        hora.hashCode ^
        local.hashCode;
  }
}
