class PedidosModel {
  String id;
  String tipo;
  DateTime dtpedido;
  DateTime data;
  String texto;
  String solicitante;
  String para;
  String imagem;

  PedidosModel(
      {this.id,
      this.tipo,
      this.dtpedido,
      this.data,
      this.texto,
      this.solicitante,
      this.para,
      this.imagem});

  PedidosModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    dtpedido = json['dtpedido'];
    data = json['data'];
    texto = json['texto'];
    solicitante = json['solicitante'];
    para = json['para'];
    imagem = json['imagem'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['dtpedido'] = this.dtpedido;
    data['data'] = this.data;
    data['texto'] = this.texto;
    data['solicitante'] = this.solicitante;
    data['para'] = this.para;
    data['imagem'] = this.imagem;
    return data;
  }
}
// tipo:
// dtpedido:
// texto:
// solicitante:
// para:
// imagem:
