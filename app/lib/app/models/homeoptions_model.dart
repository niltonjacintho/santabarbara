import 'dart:convert';

class Homeoptions {
  final String titulo;
  final String color;
  final String target;
  final int order;
  Homeoptions({
    this.titulo,
    this.color,
    this.target,
    this.order,
  });

  Homeoptions copyWith({
    String titulo,
    String color,
    String target,
    int order,
  }) {
    return Homeoptions(
      titulo: titulo ?? this.titulo,
      color: color ?? this.color,
      target: target ?? this.target,
      order: order ?? this.order,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'titulo': titulo,
      'color': color,
      'target': target,
      'order': order,
    };
  }

  static Homeoptions fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Homeoptions(
      titulo: map['titulo'],
      color: map['color'],
      target: map['target'],
      order: map['order']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  static Homeoptions fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() {
    return 'Homeoptions(titulo: $titulo, color: $color, target: $target, order: $order)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Homeoptions &&
        o.titulo == titulo &&
        o.color == color &&
        o.target == target &&
        o.order == order;
  }

  @override
  int get hashCode {
    return titulo.hashCode ^ color.hashCode ^ target.hashCode ^ order.hashCode;
  }
}
