class Pessoas {
  List<DadosPessoais> dadospessoais;

  Pessoas({this.dadospessoais});

  Pessoas.fromJson(Map<String, dynamic> json) {
    if (json['dadospessoais'] != null) {
      dadospessoais = new List<DadosPessoais>();
      json['dadospessoais'].forEach((v) {
        dadospessoais.add(new DadosPessoais.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dadospessoais != null) {
      data['dadospessoais'] =
          this.dadospessoais.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DadosPessoais {
  String id;
  String nome;
  String apelido;
  String estadocivil;
  String imagem;
  DateTime dtnascimento;
  bool dizimista;
  List<Endereco> endereco;
  Contatos contatos;
  Dadosparoquia dadosparoquia;
  Voluntario voluntario;
  List<Chat> chat;

  DadosPessoais(
      {this.id,
      this.nome,
      this.apelido,
      this.estadocivil,
      this.imagem,
      this.dtnascimento,
      this.dizimista,
      this.endereco,
      this.contatos,
      this.dadosparoquia,
      this.voluntario,
      this.chat});

  DadosPessoais.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
    apelido = json['apelido'];
    estadocivil = json['estadocivil'];
    imagem = json['imagem'];
    dtnascimento = json['dtnascimento'];
    dizimista = json['dizimista'];
    if (json['endereco'] != null) {
      endereco = new List<Endereco>();
      json['endereco'].forEach((v) {
        endereco.add(new Endereco.fromJson(v));
      });
    }
    contatos = json['contatos'] != null
        ? new Contatos.fromJson(json['contatos'])
        : null;
    dadosparoquia = json['dadosparoquia'] != null
        ? new Dadosparoquia.fromJson(json['dadosparoquia'])
        : null;
    voluntario = json['voluntario'] != null
        ? new Voluntario.fromJson(json['voluntario'])
        : null;
    if (json['chat'] != null) {
      chat = new List<Chat>();
      json['chat'].forEach((v) {
        chat.add(new Chat.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    data['apelido'] = this.apelido;
    data['estadocivil'] = this.estadocivil;
    data['imagem'] = this.imagem;
    data['dtnascimento'] = this.dtnascimento;
    data['dizimista'] = this.dizimista;
    if (this.endereco != null) {
      data['endereco'] = this.endereco.map((v) => v.toJson()).toList();
    }
    if (this.contatos != null) {
      data['contatos'] = this.contatos.toJson();
    }
    if (this.dadosparoquia != null) {
      data['dadosparoquia'] = this.dadosparoquia.toJson();
    }
    if (this.voluntario != null) {
      data['voluntario'] = this.voluntario.toJson();
    }
    if (this.chat != null) {
      data['chat'] = this.chat.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Endereco {
  String id;
  String logradouro;
  String bairro;
  String cep;
  String complemento;
  String uf;
  String cidade;

  Endereco(
      {this.id,
      this.logradouro,
      this.bairro,
      this.cep,
      this.complemento,
      this.uf,
      this.cidade});

  Endereco.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    logradouro = json['logradouro'];
    bairro = json['bairro'];
    cep = json['cep'];
    complemento = json['complemento'];
    uf = json['uf'];
    cidade = json['cidade'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['logradouro'] = this.logradouro;
    data['bairro'] = this.bairro;
    data['cep'] = this.cep;
    data['complemento'] = this.complemento;
    data['uf'] = this.uf;
    data['cidade'] = this.cidade;
    return data;
  }
}

class Contatos {
  String id;
  List<Telefone> telefone;
  List<Email> email;

  Contatos({this.id, this.telefone, this.email});

  Contatos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['telefone'] != null) {
      telefone = new List<Telefone>();
      json['telefone'].forEach((v) {
        telefone.add(new Telefone.fromJson(v));
      });
    }
    if (json['email'] != null) {
      email = new List<Email>();
      json['email'].forEach((v) {
        email.add(new Email.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.telefone != null) {
      data['telefone'] = this.telefone.map((v) => v.toJson()).toList();
    }
    if (this.email != null) {
      data['email'] = this.email.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Telefone {
  String id;
  String numero;
  int tipo;
  bool ativo;

  Telefone({this.id, this.numero, this.tipo, this.ativo});

  Telefone.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    numero = json['numero'];
    tipo = json['tipo'];
    ativo = json['ativo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['numero'] = this.numero;
    data['tipo'] = this.tipo;
    data['ativo'] = this.ativo;
    return data;
  }
}

class Email {
  String id;
  String email;
  int tipo;
  bool ativo;

  Email({this.id, this.email, this.tipo, this.ativo});

  Email.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    tipo = json['tipo'];
    ativo = json['ativo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['tipo'] = this.tipo;
    data['ativo'] = this.ativo;
    return data;
  }
}

class Dadosparoquia {
  String id;
  bool batizado;
  bool primeiracomunhao;
  bool crismado;
  List<Participa> participa;

  Dadosparoquia(
      {this.id,
      this.batizado,
      this.primeiracomunhao,
      this.crismado,
      this.participa});

  Dadosparoquia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    batizado = json['batizado'];
    primeiracomunhao = json['primeiracomunhao'];
    crismado = json['crismado'];
    if (json['participa'] != null) {
      participa = new List<Participa>();
      json['participa'].forEach((v) {
        participa.add(new Participa.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['batizado'] = this.batizado;
    data['primeiracomunhao'] = this.primeiracomunhao;
    data['crismado'] = this.crismado;
    if (this.participa != null) {
      data['participa'] = this.participa.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Participa {
  String pastoral;
  String id;
  String descricao;
  DateTime dataDesde;
  DateTime dataAte;

  Participa(
      {this.pastoral, this.id, this.descricao, this.dataDesde, this.dataAte});

  Participa.fromJson(Map<String, dynamic> json) {
    pastoral = json['pastoral'];
    id = json['id'];
    descricao = json['descricao'];
    dataDesde = json['data_desde'];
    dataAte = json['data_ate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pastoral'] = this.pastoral;
    data['id'] = this.id;
    data['descricao'] = this.descricao;
    data['data_desde'] = this.dataDesde;
    data['data_ate'] = this.dataAte;
    return data;
  }
}

class Voluntario {
  String id;
  List<Disponibilidade> disponibilidade;
  List<Pastorais> pastorais;
  String interesselivre;
  String experiencia;

  Voluntario(
      {this.id,
      this.disponibilidade,
      this.pastorais,
      this.interesselivre,
      this.experiencia});

  Voluntario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['disponibilidade'] != null) {
      disponibilidade = new List<Disponibilidade>();
      json['disponibilidade'].forEach((v) {
        disponibilidade.add(new Disponibilidade.fromJson(v));
      });
    }
    if (json['pastorais'] != null) {
      pastorais = new List<Pastorais>();
      json['pastorais'].forEach((v) {
        pastorais.add(new Pastorais.fromJson(v));
      });
    }
    interesselivre = json['interesselivre'];
    experiencia = json['experiencia'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.disponibilidade != null) {
      data['disponibilidade'] =
          this.disponibilidade.map((v) => v.toJson()).toList();
    }
    if (this.pastorais != null) {
      data['pastorais'] = this.pastorais.map((v) => v.toJson()).toList();
    }
    data['interesselivre'] = this.interesselivre;
    data['experiencia'] = this.experiencia;
    return data;
  }
}

class Disponibilidade {
  String id;
  String diahorasemana;

  Disponibilidade({this.id, this.diahorasemana});

  Disponibilidade.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    diahorasemana = json['diahorasemana'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['diahorasemana'] = this.diahorasemana;
    return data;
  }
}

class Pastorais {
  String id;
  String pastoral;

  Pastorais({this.id, this.pastoral});

  Pastorais.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pastoral = json['pastoral'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pastoral'] = this.pastoral;
    return data;
  }
}

class Chat {
  String id;
  DateTime data;
  String texto;
  String sender;

  Chat({this.id, this.data, this.texto, this.sender});

  Chat.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    data = json['data'];
    texto = json['texto'];
    sender = json['sender'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['data'] = this.data;
    data['texto'] = this.texto;
    data['sender'] = this.sender;
    return data;
  }
}
