class FormsBase {
  String id;
  String globalKey;
  String title;
  String description;
  String collection;
  dynamic dataObject;
  bool isAutoValidate;
  Datamethods datamethods;
  bool autoSave;
  ActionsButtons actionsButtons;
  List<AutorizedUsers> autorizedUsers;
  List<Fields> fields;

  FormsBase(
      {this.id,
      this.globalKey,
      this.title,
      this.description,
      this.collection,
      this.dataObject,
      this.isAutoValidate,
      this.datamethods,
      this.autoSave,
      this.actionsButtons,
      this.autorizedUsers,
      this.fields});

  FormsBase.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    globalKey = json['globalKey'];
    title = json['title'];
    description = json['description'];
    collection = json['collection'];
    dataObject = json['dataObject'];
    isAutoValidate = json['isAutoValidate'];
    datamethods = json['Datamethods'] != null
        ? new Datamethods.fromJson(json['Datamethods'])
        : null;
    autoSave = json['autoSave'];
    actionsButtons = json['actionsButtons'] != null
        ? new ActionsButtons.fromJson(json['actionsButtons'])
        : null;
    if (json['autorizedUsers'] != null) {
      autorizedUsers = new List<AutorizedUsers>();
      json['autorizedUsers'].forEach((v) {
        autorizedUsers.add(new AutorizedUsers.fromJson(v));
      });
    }
    if (json['fields'] != null) {
      fields = new List<Fields>();
      json['fields'].forEach((v) {
        fields.add(new Fields.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['globalKey'] = this.globalKey;
    data['title'] = this.title;
    data['description'] = this.description;
    data['collection'] = this.collection;
    data['dataObject'] = this.dataObject;
    data['isAutoValidate'] = this.isAutoValidate;
    if (this.datamethods != null) {
      data['Datamethods'] = this.datamethods.toJson();
    }
    data['autoSave'] = this.autoSave;
    if (this.actionsButtons != null) {
      data['actionsButtons'] = this.actionsButtons.toJson();
    }
    if (this.autorizedUsers != null) {
      data['autorizedUsers'] =
          this.autorizedUsers.map((v) => v.toJson()).toList();
    }
    if (this.fields != null) {
      data['fields'] = this.fields.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Datamethods {
  bool isUseDefault;
  String load;
  String delete;
  String save;
  String loadMore;

  Datamethods(
      {this.isUseDefault, this.load, this.delete, this.save, this.loadMore});

  Datamethods.fromJson(Map<String, dynamic> json) {
    isUseDefault = json['isUseDefault'];
    load = json['load'];
    delete = json['delete'];
    save = json['save'];
    loadMore = json['loadMore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isUseDefault'] = this.isUseDefault;
    data['load'] = this.load;
    data['delete'] = this.delete;
    data['save'] = this.save;
    data['loadMore'] = this.loadMore;
    return data;
  }
}

class ActionsButtons {
  Save save;
  Save delete;
  Save abort;

  ActionsButtons({this.save, this.delete, this.abort});

  ActionsButtons.fromJson(Map<String, dynamic> json) {
    save = json['save'] != null ? new Save.fromJson(json['save']) : null;
    delete = json['delete'] != null ? new Save.fromJson(json['delete']) : null;
    abort = json['abort'] != null ? new Save.fromJson(json['abort']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.save != null) {
      data['save'] = this.save.toJson();
    }
    if (this.delete != null) {
      data['delete'] = this.delete.toJson();
    }
    if (this.abort != null) {
      data['abort'] = this.abort.toJson();
    }
    return data;
  }
}

class Save {
  String label;
  String action;

  Save({this.label, this.action});

  Save.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    action = json['action'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    data['action'] = this.action;
    return data;
  }
}

class AutorizedUsers {
  UserName userName;

  AutorizedUsers({this.userName});

  AutorizedUsers.fromJson(Map<String, dynamic> json) {
    userName = json['userName'] != null
        ? new UserName.fromJson(json['userName'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userName != null) {
      data['userName'] = this.userName.toJson();
    }
    return data;
  }
}

class UserName {
  List<Rulles> rulles;

  UserName({this.rulles});

  UserName.fromJson(Map<String, dynamic> json) {
    if (json['rulles'] != null) {
      rulles = new List<Rulles>();
      json['rulles'].forEach((v) {
        rulles.add(new Rulles.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.rulles != null) {
      data['rulles'] = this.rulles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rulles {
  String rulle;

  Rulles({this.rulle});

  Rulles.fromJson(Map<String, dynamic> json) {
    rulle = json['rulle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rulle'] = this.rulle;
    return data;
  }
}

class Fields {
  String id;
  ListFields listFields;
  FieldType fieldType;
  bool isRequired;
  int position;
  int rowNumber;
  String placeHolder;
  String label;
  String prefixIcon;
  String suffixIcon;
  String validation;

  Fields(
      {this.id,
      this.listFields,
      this.fieldType,
      this.isRequired,
      this.position,
      this.rowNumber,
      this.placeHolder,
      this.label,
      this.prefixIcon,
      this.suffixIcon,
      this.validation});

  Fields.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    listFields = json['listFields'] != null
        ? new ListFields.fromJson(json['listFields'])
        : null;
    fieldType = json['fieldType'] != null
        ? new FieldType.fromJson(json['fieldType'])
        : null;
    isRequired = json['isRequired'];
    position = json['position'];
    rowNumber = json['rowNumber'];
    placeHolder = json['placeHolder'];
    label = json['label'];
    prefixIcon = json['prefixIcon'];
    suffixIcon = json['suffixIcon'];
    validation = json['validation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.listFields != null) {
      data['listFields'] = this.listFields.toJson();
    }
    if (this.fieldType != null) {
      data['fieldType'] = this.fieldType.toJson();
    }
    data['isRequired'] = this.isRequired;
    data['position'] = this.position;
    data['rowNumber'] = this.rowNumber;
    data['placeHolder'] = this.placeHolder;
    data['label'] = this.label;
    data['prefixIcon'] = this.prefixIcon;
    data['suffixIcon'] = this.suffixIcon;
    data['validation'] = this.validation;
    return data;
  }
}

class ListFields {
  bool isExistInList;
  int order;
  int listPosition;

  ListFields({this.isExistInList, this.order, this.listPosition});

  ListFields.fromJson(Map<String, dynamic> json) {
    isExistInList = json['isExistInList'];
    order = json['order'];
    listPosition = json['listPosition'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isExistInList'] = this.isExistInList;
    data['order'] = this.order;
    data['listPosition'] = this.listPosition;
    return data;
  }
}

class FieldType {
  String name;

  FieldType({this.name});

  FieldType.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}
