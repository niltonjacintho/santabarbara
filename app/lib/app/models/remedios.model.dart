class Remedios {
  String id;
  String nome;
  String precomedio;
  String dataReferencia;
  Bula bula;
  List<Fabricantes> fabricantes;

  Remedios(
      {this.id,
      this.nome,
      this.precomedio,
      this.dataReferencia,
      this.bula,
      this.fabricantes});

  Remedios.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
    precomedio = json['precomedio'];
    dataReferencia = json['dataReferencia'];
    bula = json['bula'] != null ? new Bula.fromJson(json['bula']) : null;
    if (json['fabricantes'] != null) {
      fabricantes = new List<Fabricantes>();
      json['fabricantes'].forEach((v) {
        fabricantes.add(new Fabricantes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nome'] = this.nome;
    data['id'] = this.id;
    data['precomedio'] = this.precomedio;
    data['dataReferencia'] = this.dataReferencia;
    if (this.bula != null) {
      data['bula'] = this.bula.toJson();
    }
    if (this.fabricantes != null) {
      data['fabricantes'] = this.fabricantes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Bula {
  String paraQueServe;
  String contraIndicacao;
  String comoUsar;
  String precaucoes;
  String composicao;
  String legais;

  Bula(
      {this.paraQueServe,
      this.contraIndicacao,
      this.comoUsar,
      this.precaucoes,
      this.composicao,
      this.legais});

  Bula.fromJson(Map<String, dynamic> json) {
    paraQueServe = json['paraQueServe'];
    contraIndicacao = json['contraIndicacao'];
    comoUsar = json['comoUsar'];
    precaucoes = json['precaucoes'];
    composicao = json['composicao'];
    legais = json['legais'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paraQueServe'] = this.paraQueServe;
    data['contraIndicacao'] = this.contraIndicacao;
    data['comoUsar'] = this.comoUsar;
    data['precaucoes'] = this.precaucoes;
    data['composicao'] = this.composicao;
    data['legais'] = this.legais;
    return data;
  }
}

class Fabricantes {
  String nome;

  Fabricantes({this.nome});

  Fabricantes.fromJson(Map<String, dynamic> json) {
    nome = json['nome'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nome'] = this.nome;
    return data;
  }
}
