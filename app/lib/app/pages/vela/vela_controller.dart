import 'package:mobx/mobx.dart';

part 'vela_controller.g.dart';

class VelaController = _VelaControllerBase with _$VelaController;

abstract class _VelaControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
