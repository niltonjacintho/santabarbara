import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class VelaPage extends StatefulWidget {
  final String title;
  const VelaPage({Key key, this.title = "Vela"}) : super(key: key);

  @override
  _VelaPageState createState() => _VelaPageState();
}

class _VelaPageState extends State<VelaPage> {
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          FittedBox(
            child: Image.asset(
              "assets/images/vela1.gif",
            ),
            fit: BoxFit.fill,
          ),
          Positioned(
            bottom: 90,
            child: SizedBox(
              width: 250.0,
              child: FadeAnimatedTextKit(
                  onTap: () {
                    print("Tap Event");
                  },
                  text: [""],
                  textStyle: TextStyle(
                      fontSize: 32.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.start,
                  // alignment:
                  //     AlignmentDirectional.topStart // or Alignment.topLeft
                  ),
            ),
            // child: Center(
            //   child: GestureDetector(
            //     onTap: () {
            //       setState(() {
            //         selected = !selected;
            //       });
            //     },
            //     child: AnimatedDefaultTextStyle(
            //       style: selected
            //           ? GoogleFonts.kalam(fontSize: 60, color: Colors.white)
            //           : GoogleFonts.kalam(fontSize: 35, color: Colors.blue),
            //       duration: const Duration(seconds: 2),
            //       child: Center(child: Text('Nome da Pessoa')),
            //     ),
            //   ),
            // ),
          ),
        ],
      ),
    );
  }
}
