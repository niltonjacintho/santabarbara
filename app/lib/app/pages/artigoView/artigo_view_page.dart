import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'artigo_view_controller.dart';
import 'package:get_storage/get_storage.dart';
import 'package:santabarbara/app/modules/administrator/documents/documents_controller.dart';
import 'package:santabarbara/app/shared/utils/utils_controller.dart';

class ArtigoViewPage extends StatefulWidget {
  final String title;
  const ArtigoViewPage({Key key, this.title}) : super(key: key);

  @override
  _ArtigoViewPageState createState() => _ArtigoViewPageState();
}

class _ArtigoViewPageState
    extends ModularState<ArtigoViewPage, ArtigoViewController> {
  // _ArtigoViewPageState();
  UtilsController utilsController = Modular.get();
  //use 'controller' variable to access controller
  final box = GetStorage();
  double tamanhoFonte = GetStorage().read('tamanhoFonte') ?? 10;
  DocumentsController documentsController = Modular.get();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          FlatButton(
            color: Colors.transparent,
            splashColor: Colors.black26,
            onPressed: () {
              setState(() {
                tamanhoFonte++;
                tamanhoFonte = tamanhoFonte > 50 ? 50 : tamanhoFonte;
                box.write('tamanhoFonte', tamanhoFonte);
                // tamanhoFonte = tamanho;
              });
            },
            child: Text('A+',
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                    color: Colors.white)),
          ),
          FlatButton(
            color: Colors.transparent,
            splashColor: Colors.black26,
            onPressed: () {
              setState(() {
                tamanhoFonte--;
                tamanhoFonte = tamanhoFonte < 12 ? 12 : tamanhoFonte;
                box.write('tamanhoFonte', tamanhoFonte);
                // tamanhoFonte = tamanho;
              });
            },
            child: Text('A-',
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                    color: Colors.white)),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            // Slider(
            //   value: tamanhoFonte,
            //   min: 10,
            //   max: 40,
            //   divisions: 5,
            //   label: tamanhoFonte.toString(),
            //   // min: 10,
            //   onChanged: (tamanho) {
            //     setState(() {
            //       box.write('tamanhoFonte', tamanho);
            //       tamanhoFonte = tamanho;
            //     });
            //   },
            // ),
            SizedBox(
              child: documentsController.documents.imagem != ""
                  ? Image.network(
                      documentsController.documents.imagem,
                      fit: BoxFit.fitHeight,
                      alignment: Alignment.topCenter,
                    )
                  : Text(''),
              height: documentsController.documents.imagem != "" ? 300 : 10,
              width: documentsController.documents.imagem != "" ? 400 : 10,
            ),
            Text(
              documentsController.documents.titulo,
              style: TextStyle(
                color: Colors.grey[800],
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Text(
                documentsController.documents.conteudo,
                style: TextStyle(fontSize: tamanhoFonte),
                strutStyle: StrutStyle(fontSize: 13),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
