import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'artigo_view_controller.g.dart';

@Injectable()
class ArtigoViewController = _ArtigoViewControllerBase
    with _$ArtigoViewController;

abstract class _ArtigoViewControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
