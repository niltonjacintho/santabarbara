import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'oracao_controller.g.dart';

@Injectable()
class OracaoController = _OracaoControllerBase with _$OracaoController;

abstract class _OracaoControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
