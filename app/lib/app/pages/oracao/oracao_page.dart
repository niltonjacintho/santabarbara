import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/pages/oracao/oracao_controller.dart';
import 'package:santabarbara/app/store/base_store.dart';
import 'package:get_storage/get_storage.dart';

class OracaoPage extends StatefulWidget {
  final String title;
  const OracaoPage({Key key, this.title = "Oracao"}) : super(key: key);

  @override
  _OracaoPageState createState() => _OracaoPageState();
}

class _OracaoPageState extends ModularState<OracaoPage, OracaoController> {
  //use 'controller' variable to access controller

  BaseStore baseStore = Modular.get();

  @override
  Widget build(BuildContext context) {
    double tamanhoFonte = GetStorage().read('tamanhoFonte') ?? 10;
    final box = GetStorage();
    return Scaffold(
      appBar: AppBar(
        title: Text(baseStore.oracao.titulo),
        actions: <Widget>[
          FlatButton(
            color: Colors.transparent,
            splashColor: Colors.black26,
            onPressed: () {
              setState(() {
                tamanhoFonte++;
                tamanhoFonte = tamanhoFonte > 50 ? 50 : tamanhoFonte;
                box.write('tamanhoFonte', tamanhoFonte);
                // tamanhoFonte = tamanho;
              });
            },
            child: Text('A+',
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                    color: Colors.white)),
          ),
          FlatButton(
            color: Colors.transparent,
            splashColor: Colors.black26,
            onPressed: () {
              setState(() {
                tamanhoFonte--;
                tamanhoFonte = tamanhoFonte < 12 ? 12 : tamanhoFonte;
                box.write('tamanhoFonte', tamanhoFonte);
                // tamanhoFonte = tamanho;
              });
            },
            child: Text(
              'A-',
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w900,
                  color: Colors.white),
              strutStyle: StrutStyle(fontSize: 13),
            ),
          ),
        ],
      ),
      //),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Text(
              baseStore.oracao.texto,
              style: TextStyle(fontSize: tamanhoFonte),
            )
          ],
        ),
      ),
    );
  }
}
