import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  startTime() async {
    print('START TIMEED');
    var duration = new Duration(seconds: 6);
    return new Timer(duration, route);
  }

  bool selected = false;
  @override
  Widget build(BuildContext context) {
    startTime();
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: Container(
        decoration: BoxDecoration(
          // borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.red,
        ),
        child: Center(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.asset(
              'assets/images/santabarbara.jpg',
              width: 350.0,
              height: 510.0,
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
    );
  }

  route() {
    Modular.to.pushReplacementNamed('/login');
    // Modular.to.pushReplacementNamed('/quiztop');
  }
}
