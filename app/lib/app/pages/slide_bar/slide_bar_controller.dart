import 'package:mobx/mobx.dart';

part 'slide_bar_controller.g.dart';

class SlideBarController = _SlideBarControllerBase with _$SlideBarController;

abstract class _SlideBarControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
