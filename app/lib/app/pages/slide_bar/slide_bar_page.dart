import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:santabarbara/app/modules/agenda/agenda_controller.dart';
import 'package:santabarbara/app/modules/paroquias/paroquias_controller.dart';
import 'package:santabarbara/app/modules/quiz/quiz_controller.dart';
import 'package:santabarbara/app/shared/auth/auth_controller.dart';
import 'package:santabarbara/app/shared/igreja.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AgendaController agendaController = Modular.get();
    ParoquiasController paroquiasController = Modular.get();
    QuizController quizController = Modular.get();
    CargaIgreja cargaIgreja = new CargaIgreja();
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            _createHeader(),
            Divider(height: 10),
            GestureDetector(
              onTap: () => {agendaController.cleanData()},
              child: _createDrawerItem(
                icon: Icons.contacts,
                text: 'Dados pessoais',
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () => {paroquiasController.atualizarLocation()},
              child: _createDrawerItem(
                icon: Icons.contacts,
                text: 'Ajustar dados das paróquias',
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () => {quizController.carregarPerguntas()},
              child: _createDrawerItem(
                icon: Icons.contacts,
                text: 'Carregar dados do QUIZ',
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () => {quizController.limparBase()},
              child: _createDrawerItem(
                icon: Icons.contacts,
                text: 'Limpar dados Quiz',
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () => {cargaIgreja.criar()},
              child: _createDrawerItem(
                icon: Icons.contacts,
                text: 'Carregar Paróquias',
              ),
            ),
            Divider(),
            ListTile(
              title: Text('0.0.1'),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }

  Widget _createDrawerItem(
      {IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }

  Widget _createHeader() {
    AuthController authController = Modular.get();
    return DrawerHeader(
      child: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Image.network(
                  authController.user.providerData[0].photoURL,
                  scale: 2,
                ),
              ),
              Center(
                child: Text(
                  authController.user.displayName,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                ),
              ),
              Center(
                child: Text(
                  authController.user.providerData[0].email == null
                      ? ''
                      : authController.user.providerData[0].email,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
