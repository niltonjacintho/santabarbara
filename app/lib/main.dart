import 'package:flutter/material.dart';
import 'package:santabarbara/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:firebase_core/firebase_core.dart';
//import 'package:get/get.dart';

import 'package:get_storage/get_storage.dart';
import 'package:santabarbara/app/app_widget.dart';
import 'package:santabarbara/app/modules/login/login_module.dart';
import 'package:santabarbara/app/modules/login/login_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  runApp(ModularApp(
    module: AppModule(),
    child: AppWidget(),
  ));
  // runApp(ModularApp(
  //     child: MediaQuery(
  //         data: new MediaQueryData(),
  //         child: new MaterialApp(home: new LoginPage())),
  //     module: AppModule()));
}
// void main() => initializeDateFormatting().then(
//       (_) => {print('111'), runApp()},
//     );
