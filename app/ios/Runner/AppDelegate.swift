import UIKit
import Flutter
Import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    GMServices.provideAPIKey("AIzaSyDikppRvC3xGB2Jikx_u_STV5kCGUYP51M")
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
